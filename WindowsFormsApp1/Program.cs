﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Initialize a new thread
            // This thread starts GUI Form1
            Thread A = new Thread(RunGUI);

            // Start and join the thread
            A.Start();
            A.Join();
        }
        
        // Create a instance of Form1 
        public static Form1 GUI = new Form1();
        
        // Used to show progress bar amount
        delegate void progresscheck(int input);

        // To tell program that calculation is done
        delegate void finish();
        
        static void RunGUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(GUI);
        }

        public static void finished()
        {
            if (Program.GUI.groupBox.InvokeRequired)
            {
                finish findelegate = new finish(finish_check_method);
                GUI.groupBox.BeginInvoke(findelegate);
            }
            else
            {
                finish_check_method();
            }
        }

        public static void increase(int input)
        {
            if (Program.GUI.progressBar_Calculation.InvokeRequired)
            {
                progresscheck progressdelegate = new progresscheck(progress_check_method);
                GUI.progressBar_Calculation.BeginInvoke(progressdelegate, input);
            }
            else
            {
                // If invoke is not necessary
                progress_check_method(input);
            }
        }

        public static void progress_check_method(int amount)
        {
            GUI.progressBar_Calculation.Value += amount;
            GUI.progressBar_Calculation.Update();
        }
        
        public static void finish_check_method()
        {
            GUI.finish();
        }
    }
}
