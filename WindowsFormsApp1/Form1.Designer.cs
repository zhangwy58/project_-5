﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.errorProvider_S = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_K = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_r = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_T = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_sigma = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_N = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider_trials = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButton_EuropeanCall = new System.Windows.Forms.RadioButton();
            this.radioButton_EuropeanPut = new System.Windows.Forms.RadioButton();
            this.label_OptionResult = new System.Windows.Forms.Label();
            this.label_Delta = new System.Windows.Forms.Label();
            this.label_DeltaResult = new System.Windows.Forms.Label();
            this.label_Gamma = new System.Windows.Forms.Label();
            this.label_GammaResult = new System.Windows.Forms.Label();
            this.label_Vega = new System.Windows.Forms.Label();
            this.label_VegaResult = new System.Windows.Forms.Label();
            this.label_Theta = new System.Windows.Forms.Label();
            this.label_ThetaResult = new System.Windows.Forms.Label();
            this.label_Rho = new System.Windows.Forms.Label();
            this.label_RhoResult = new System.Windows.Forms.Label();
            this.label_SE = new System.Windows.Forms.Label();
            this.label_SEResult = new System.Windows.Forms.Label();
            this.label_Time = new System.Windows.Forms.Label();
            this.label_TimeResult = new System.Windows.Forms.Label();
            this.progressBar_Calculation = new System.Windows.Forms.ProgressBar();
            this.label_Cores = new System.Windows.Forms.Label();
            this.label_CoresResult = new System.Windows.Forms.Label();
            this.label_optionPrice = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.radioButton_AsianCall = new System.Windows.Forms.RadioButton();
            this.radioButton_AsianPut = new System.Windows.Forms.RadioButton();
            this.errorProvider_rebate = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButton_DigitalCall = new System.Windows.Forms.RadioButton();
            this.radioButton_DigitalPut = new System.Windows.Forms.RadioButton();
            this.radioButton_BarrierCall = new System.Windows.Forms.RadioButton();
            this.radioButton_BarrierPut = new System.Windows.Forms.RadioButton();
            this.radioButton_LookbackCall = new System.Windows.Forms.RadioButton();
            this.radioButton_LookbackPut = new System.Windows.Forms.RadioButton();
            this.radioButton_RangeOption = new System.Windows.Forms.RadioButton();
            this.checkBox_Antithetic = new System.Windows.Forms.CheckBox();
            this.checkBox_MT = new System.Windows.Forms.CheckBox();
            this.checkBox_CV = new System.Windows.Forms.CheckBox();
            this.errorProvider_barrier = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButton_DO = new System.Windows.Forms.RadioButton();
            this.radioButton_UO = new System.Windows.Forms.RadioButton();
            this.radioButton_UI = new System.Windows.Forms.RadioButton();
            this.radioButton_DI = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_trials = new System.Windows.Forms.TextBox();
            this.label_trials = new System.Windows.Forms.Label();
            this.label_rebate = new System.Windows.Forms.Label();
            this.label_N = new System.Windows.Forms.Label();
            this.textBox_rebate = new System.Windows.Forms.TextBox();
            this.textBox_N = new System.Windows.Forms.TextBox();
            this.textBox_sigma = new System.Windows.Forms.TextBox();
            this.label_sigma = new System.Windows.Forms.Label();
            this.textBox_T = new System.Windows.Forms.TextBox();
            this.label_T = new System.Windows.Forms.Label();
            this.textBox_r = new System.Windows.Forms.TextBox();
            this.label_r = new System.Windows.Forms.Label();
            this.label_K = new System.Windows.Forms.Label();
            this.label_barrier = new System.Windows.Forms.Label();
            this.textBox_K = new System.Windows.Forms.TextBox();
            this.textBox_barrier = new System.Windows.Forms.TextBox();
            this.textBox_S = new System.Windows.Forms.TextBox();
            this.label_S = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_r)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_T)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_sigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_trials)).BeginInit();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_rebate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_barrier)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_calculate
            // 
            this.btn_calculate.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_calculate.Location = new System.Drawing.Point(647, 696);
            this.btn_calculate.Margin = new System.Windows.Forms.Padding(4);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(283, 73);
            this.btn_calculate.TabIndex = 3;
            this.btn_calculate.Text = "Calculate";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.btn_calculate_Click);
            // 
            // errorProvider_S
            // 
            this.errorProvider_S.ContainerControl = this;
            // 
            // errorProvider_K
            // 
            this.errorProvider_K.ContainerControl = this;
            // 
            // errorProvider_r
            // 
            this.errorProvider_r.ContainerControl = this;
            // 
            // errorProvider_T
            // 
            this.errorProvider_T.ContainerControl = this;
            // 
            // errorProvider_sigma
            // 
            this.errorProvider_sigma.ContainerControl = this;
            // 
            // errorProvider_N
            // 
            this.errorProvider_N.ContainerControl = this;
            // 
            // errorProvider_trials
            // 
            this.errorProvider_trials.ContainerControl = this;
            // 
            // radioButton_EuropeanCall
            // 
            this.radioButton_EuropeanCall.AutoSize = true;
            this.radioButton_EuropeanCall.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_EuropeanCall.Location = new System.Drawing.Point(33, 701);
            this.radioButton_EuropeanCall.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton_EuropeanCall.Name = "radioButton_EuropeanCall";
            this.radioButton_EuropeanCall.Size = new System.Drawing.Size(198, 23);
            this.radioButton_EuropeanCall.TabIndex = 15;
            this.radioButton_EuropeanCall.TabStop = true;
            this.radioButton_EuropeanCall.Text = "European Call Option";
            this.radioButton_EuropeanCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_EuropeanPut
            // 
            this.radioButton_EuropeanPut.AutoSize = true;
            this.radioButton_EuropeanPut.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_EuropeanPut.Location = new System.Drawing.Point(33, 750);
            this.radioButton_EuropeanPut.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton_EuropeanPut.Name = "radioButton_EuropeanPut";
            this.radioButton_EuropeanPut.Size = new System.Drawing.Size(196, 23);
            this.radioButton_EuropeanPut.TabIndex = 17;
            this.radioButton_EuropeanPut.TabStop = true;
            this.radioButton_EuropeanPut.Text = "European Put Option";
            this.radioButton_EuropeanPut.UseVisualStyleBackColor = true;
            // 
            // label_OptionResult
            // 
            this.label_OptionResult.AutoSize = true;
            this.label_OptionResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_OptionResult.Location = new System.Drawing.Point(243, 52);
            this.label_OptionResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_OptionResult.Name = "label_OptionResult";
            this.label_OptionResult.Size = new System.Drawing.Size(111, 22);
            this.label_OptionResult.TabIndex = 20;
            this.label_OptionResult.Text = "Option Price";
            // 
            // label_Delta
            // 
            this.label_Delta.AutoSize = true;
            this.label_Delta.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Delta.Location = new System.Drawing.Point(87, 108);
            this.label_Delta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Delta.Name = "label_Delta";
            this.label_Delta.Size = new System.Drawing.Size(108, 22);
            this.label_Delta.TabIndex = 21;
            this.label_Delta.Text = "Delta Value:";
            // 
            // label_DeltaResult
            // 
            this.label_DeltaResult.AutoSize = true;
            this.label_DeltaResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DeltaResult.Location = new System.Drawing.Point(248, 105);
            this.label_DeltaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_DeltaResult.Name = "label_DeltaResult";
            this.label_DeltaResult.Size = new System.Drawing.Size(102, 22);
            this.label_DeltaResult.TabIndex = 22;
            this.label_DeltaResult.Text = "Delta Value";
            // 
            // label_Gamma
            // 
            this.label_Gamma.AutoSize = true;
            this.label_Gamma.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Gamma.Location = new System.Drawing.Point(67, 164);
            this.label_Gamma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Gamma.Name = "label_Gamma";
            this.label_Gamma.Size = new System.Drawing.Size(125, 22);
            this.label_Gamma.TabIndex = 23;
            this.label_Gamma.Text = "Gamma Value:";
            // 
            // label_GammaResult
            // 
            this.label_GammaResult.AutoSize = true;
            this.label_GammaResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_GammaResult.Location = new System.Drawing.Point(248, 161);
            this.label_GammaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_GammaResult.Name = "label_GammaResult";
            this.label_GammaResult.Size = new System.Drawing.Size(119, 22);
            this.label_GammaResult.TabIndex = 24;
            this.label_GammaResult.Text = "Gamma Value";
            // 
            // label_Vega
            // 
            this.label_Vega.AutoSize = true;
            this.label_Vega.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Vega.Location = new System.Drawing.Point(89, 218);
            this.label_Vega.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Vega.Name = "label_Vega";
            this.label_Vega.Size = new System.Drawing.Size(103, 22);
            this.label_Vega.TabIndex = 25;
            this.label_Vega.Text = "Vega Value:";
            // 
            // label_VegaResult
            // 
            this.label_VegaResult.AutoSize = true;
            this.label_VegaResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VegaResult.Location = new System.Drawing.Point(248, 215);
            this.label_VegaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_VegaResult.Name = "label_VegaResult";
            this.label_VegaResult.Size = new System.Drawing.Size(97, 22);
            this.label_VegaResult.TabIndex = 26;
            this.label_VegaResult.Text = "Vega Value";
            // 
            // label_Theta
            // 
            this.label_Theta.AutoSize = true;
            this.label_Theta.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Theta.Location = new System.Drawing.Point(83, 279);
            this.label_Theta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Theta.Name = "label_Theta";
            this.label_Theta.Size = new System.Drawing.Size(109, 22);
            this.label_Theta.TabIndex = 27;
            this.label_Theta.Text = "Theta Value:";
            // 
            // label_ThetaResult
            // 
            this.label_ThetaResult.AutoSize = true;
            this.label_ThetaResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ThetaResult.Location = new System.Drawing.Point(248, 276);
            this.label_ThetaResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_ThetaResult.Name = "label_ThetaResult";
            this.label_ThetaResult.Size = new System.Drawing.Size(103, 22);
            this.label_ThetaResult.TabIndex = 28;
            this.label_ThetaResult.Text = "Theta Value";
            // 
            // label_Rho
            // 
            this.label_Rho.AutoSize = true;
            this.label_Rho.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Rho.Location = new System.Drawing.Point(95, 332);
            this.label_Rho.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Rho.Name = "label_Rho";
            this.label_Rho.Size = new System.Drawing.Size(97, 22);
            this.label_Rho.TabIndex = 29;
            this.label_Rho.Text = "Rho Value:";
            // 
            // label_RhoResult
            // 
            this.label_RhoResult.AutoSize = true;
            this.label_RhoResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RhoResult.Location = new System.Drawing.Point(248, 335);
            this.label_RhoResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_RhoResult.Name = "label_RhoResult";
            this.label_RhoResult.Size = new System.Drawing.Size(91, 22);
            this.label_RhoResult.TabIndex = 30;
            this.label_RhoResult.Text = "Rho Value";
            // 
            // label_SE
            // 
            this.label_SE.AutoSize = true;
            this.label_SE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SE.Location = new System.Drawing.Point(152, 392);
            this.label_SE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_SE.Name = "label_SE";
            this.label_SE.Size = new System.Drawing.Size(39, 22);
            this.label_SE.TabIndex = 31;
            this.label_SE.Text = "SE:";
            // 
            // label_SEResult
            // 
            this.label_SEResult.AutoSize = true;
            this.label_SEResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SEResult.Location = new System.Drawing.Point(248, 389);
            this.label_SEResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_SEResult.Name = "label_SEResult";
            this.label_SEResult.Size = new System.Drawing.Size(82, 22);
            this.label_SEResult.TabIndex = 32;
            this.label_SEResult.Text = "SE Value";
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Time.Location = new System.Drawing.Point(138, 440);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(61, 22);
            this.label_Time.TabIndex = 36;
            this.label_Time.Text = "Time: ";
            // 
            // label_TimeResult
            // 
            this.label_TimeResult.AutoSize = true;
            this.label_TimeResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TimeResult.Location = new System.Drawing.Point(248, 437);
            this.label_TimeResult.Name = "label_TimeResult";
            this.label_TimeResult.Size = new System.Drawing.Size(99, 22);
            this.label_TimeResult.TabIndex = 37;
            this.label_TimeResult.Text = "Time Value";
            // 
            // progressBar_Calculation
            // 
            this.progressBar_Calculation.Location = new System.Drawing.Point(526, 660);
            this.progressBar_Calculation.Maximum = 8;
            this.progressBar_Calculation.Name = "progressBar_Calculation";
            this.progressBar_Calculation.Size = new System.Drawing.Size(448, 23);
            this.progressBar_Calculation.TabIndex = 39;
            // 
            // label_Cores
            // 
            this.label_Cores.AutoSize = true;
            this.label_Cores.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Cores.Location = new System.Drawing.Point(98, 486);
            this.label_Cores.Name = "label_Cores";
            this.label_Cores.Size = new System.Drawing.Size(95, 22);
            this.label_Cores.TabIndex = 40;
            this.label_Cores.Text = "# of cores:";
            // 
            // label_CoresResult
            // 
            this.label_CoresResult.AutoSize = true;
            this.label_CoresResult.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CoresResult.Location = new System.Drawing.Point(248, 484);
            this.label_CoresResult.Name = "label_CoresResult";
            this.label_CoresResult.Size = new System.Drawing.Size(112, 22);
            this.label_CoresResult.TabIndex = 41;
            this.label_CoresResult.Text = "Cores Result";
            // 
            // label_optionPrice
            // 
            this.label_optionPrice.AutoSize = true;
            this.label_optionPrice.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_optionPrice.Location = new System.Drawing.Point(75, 54);
            this.label_optionPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_optionPrice.Name = "label_optionPrice";
            this.label_optionPrice.Size = new System.Drawing.Size(117, 22);
            this.label_optionPrice.TabIndex = 19;
            this.label_optionPrice.Text = "Option Price:";
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.label_optionPrice);
            this.groupBox.Controls.Add(this.label_Vega);
            this.groupBox.Controls.Add(this.label_TimeResult);
            this.groupBox.Controls.Add(this.label_Gamma);
            this.groupBox.Controls.Add(this.label_CoresResult);
            this.groupBox.Controls.Add(this.label_Delta);
            this.groupBox.Controls.Add(this.label_Cores);
            this.groupBox.Controls.Add(this.label_SEResult);
            this.groupBox.Controls.Add(this.label_RhoResult);
            this.groupBox.Controls.Add(this.label_ThetaResult);
            this.groupBox.Controls.Add(this.label_VegaResult);
            this.groupBox.Controls.Add(this.label_GammaResult);
            this.groupBox.Controls.Add(this.label_DeltaResult);
            this.groupBox.Controls.Add(this.label_OptionResult);
            this.groupBox.Controls.Add(this.label_Theta);
            this.groupBox.Controls.Add(this.label_Rho);
            this.groupBox.Controls.Add(this.label_SE);
            this.groupBox.Controls.Add(this.label_Time);
            this.groupBox.Location = new System.Drawing.Point(505, 12);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(456, 540);
            this.groupBox.TabIndex = 42;
            this.groupBox.TabStop = false;
            // 
            // radioButton_AsianCall
            // 
            this.radioButton_AsianCall.AutoSize = true;
            this.radioButton_AsianCall.Location = new System.Drawing.Point(240, 701);
            this.radioButton_AsianCall.Name = "radioButton_AsianCall";
            this.radioButton_AsianCall.Size = new System.Drawing.Size(165, 23);
            this.radioButton_AsianCall.TabIndex = 43;
            this.radioButton_AsianCall.TabStop = true;
            this.radioButton_AsianCall.Text = "Asian Call Option";
            this.radioButton_AsianCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_AsianPut
            // 
            this.radioButton_AsianPut.AutoSize = true;
            this.radioButton_AsianPut.Location = new System.Drawing.Point(240, 749);
            this.radioButton_AsianPut.Name = "radioButton_AsianPut";
            this.radioButton_AsianPut.Size = new System.Drawing.Size(163, 23);
            this.radioButton_AsianPut.TabIndex = 44;
            this.radioButton_AsianPut.TabStop = true;
            this.radioButton_AsianPut.Text = "Asian Put Option";
            this.radioButton_AsianPut.UseVisualStyleBackColor = true;
            // 
            // errorProvider_rebate
            // 
            this.errorProvider_rebate.ContainerControl = this;
            // 
            // radioButton_DigitalCall
            // 
            this.radioButton_DigitalCall.AutoSize = true;
            this.radioButton_DigitalCall.Location = new System.Drawing.Point(448, 568);
            this.radioButton_DigitalCall.Name = "radioButton_DigitalCall";
            this.radioButton_DigitalCall.Size = new System.Drawing.Size(171, 23);
            this.radioButton_DigitalCall.TabIndex = 47;
            this.radioButton_DigitalCall.TabStop = true;
            this.radioButton_DigitalCall.Text = "Digital Call Option";
            this.radioButton_DigitalCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_DigitalPut
            // 
            this.radioButton_DigitalPut.AutoSize = true;
            this.radioButton_DigitalPut.Location = new System.Drawing.Point(448, 615);
            this.radioButton_DigitalPut.Name = "radioButton_DigitalPut";
            this.radioButton_DigitalPut.Size = new System.Drawing.Size(169, 23);
            this.radioButton_DigitalPut.TabIndex = 48;
            this.radioButton_DigitalPut.TabStop = true;
            this.radioButton_DigitalPut.Text = "Digital Put Option";
            this.radioButton_DigitalPut.UseVisualStyleBackColor = true;
            // 
            // radioButton_BarrierCall
            // 
            this.radioButton_BarrierCall.AutoSize = true;
            this.radioButton_BarrierCall.Location = new System.Drawing.Point(630, 568);
            this.radioButton_BarrierCall.Name = "radioButton_BarrierCall";
            this.radioButton_BarrierCall.Size = new System.Drawing.Size(177, 23);
            this.radioButton_BarrierCall.TabIndex = 49;
            this.radioButton_BarrierCall.TabStop = true;
            this.radioButton_BarrierCall.Text = "Barrier Call Option";
            this.radioButton_BarrierCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_BarrierPut
            // 
            this.radioButton_BarrierPut.AutoSize = true;
            this.radioButton_BarrierPut.Location = new System.Drawing.Point(630, 616);
            this.radioButton_BarrierPut.Name = "radioButton_BarrierPut";
            this.radioButton_BarrierPut.Size = new System.Drawing.Size(175, 23);
            this.radioButton_BarrierPut.TabIndex = 50;
            this.radioButton_BarrierPut.TabStop = true;
            this.radioButton_BarrierPut.Text = "Barrier Put Option";
            this.radioButton_BarrierPut.UseVisualStyleBackColor = true;
            // 
            // radioButton_LookbackCall
            // 
            this.radioButton_LookbackCall.AutoSize = true;
            this.radioButton_LookbackCall.Location = new System.Drawing.Point(809, 567);
            this.radioButton_LookbackCall.Name = "radioButton_LookbackCall";
            this.radioButton_LookbackCall.Size = new System.Drawing.Size(198, 23);
            this.radioButton_LookbackCall.TabIndex = 51;
            this.radioButton_LookbackCall.TabStop = true;
            this.radioButton_LookbackCall.Text = "Lookback Call Option";
            this.radioButton_LookbackCall.UseVisualStyleBackColor = true;
            // 
            // radioButton_LookbackPut
            // 
            this.radioButton_LookbackPut.AutoSize = true;
            this.radioButton_LookbackPut.Location = new System.Drawing.Point(810, 617);
            this.radioButton_LookbackPut.Name = "radioButton_LookbackPut";
            this.radioButton_LookbackPut.Size = new System.Drawing.Size(196, 23);
            this.radioButton_LookbackPut.TabIndex = 52;
            this.radioButton_LookbackPut.Text = "Lookback Put Option";
            this.radioButton_LookbackPut.UseVisualStyleBackColor = true;
            // 
            // radioButton_RangeOption
            // 
            this.radioButton_RangeOption.AutoSize = true;
            this.radioButton_RangeOption.Location = new System.Drawing.Point(425, 701);
            this.radioButton_RangeOption.Name = "radioButton_RangeOption";
            this.radioButton_RangeOption.Size = new System.Drawing.Size(138, 23);
            this.radioButton_RangeOption.TabIndex = 53;
            this.radioButton_RangeOption.TabStop = true;
            this.radioButton_RangeOption.Text = "Range Option";
            this.radioButton_RangeOption.UseVisualStyleBackColor = true;
            // 
            // checkBox_Antithetic
            // 
            this.checkBox_Antithetic.AutoSize = true;
            this.checkBox_Antithetic.Location = new System.Drawing.Point(34, 629);
            this.checkBox_Antithetic.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_Antithetic.Name = "checkBox_Antithetic";
            this.checkBox_Antithetic.Size = new System.Drawing.Size(111, 23);
            this.checkBox_Antithetic.TabIndex = 33;
            this.checkBox_Antithetic.Text = "Antithetic ";
            this.checkBox_Antithetic.UseVisualStyleBackColor = true;
            // 
            // checkBox_MT
            // 
            this.checkBox_MT.AutoSize = true;
            this.checkBox_MT.Location = new System.Drawing.Point(34, 585);
            this.checkBox_MT.Name = "checkBox_MT";
            this.checkBox_MT.Size = new System.Drawing.Size(153, 23);
            this.checkBox_MT.TabIndex = 38;
            this.checkBox_MT.Text = "Multi-Threading";
            this.checkBox_MT.UseVisualStyleBackColor = true;
            // 
            // checkBox_CV
            // 
            this.checkBox_CV.AutoSize = true;
            this.checkBox_CV.Location = new System.Drawing.Point(34, 545);
            this.checkBox_CV.Name = "checkBox_CV";
            this.checkBox_CV.Size = new System.Drawing.Size(149, 23);
            this.checkBox_CV.TabIndex = 35;
            this.checkBox_CV.Tag = "";
            this.checkBox_CV.Text = "Control Variate";
            this.checkBox_CV.UseVisualStyleBackColor = true;
            // 
            // errorProvider_barrier
            // 
            this.errorProvider_barrier.ContainerControl = this;
            // 
            // radioButton_DO
            // 
            this.radioButton_DO.AutoSize = true;
            this.radioButton_DO.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_DO.Location = new System.Drawing.Point(22, 78);
            this.radioButton_DO.Name = "radioButton_DO";
            this.radioButton_DO.Size = new System.Drawing.Size(134, 25);
            this.radioButton_DO.TabIndex = 57;
            this.radioButton_DO.TabStop = true;
            this.radioButton_DO.Text = "Down and Out";
            this.radioButton_DO.UseVisualStyleBackColor = true;
            // 
            // radioButton_UO
            // 
            this.radioButton_UO.AutoSize = true;
            this.radioButton_UO.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_UO.Location = new System.Drawing.Point(22, 150);
            this.radioButton_UO.Name = "radioButton_UO";
            this.radioButton_UO.Size = new System.Drawing.Size(112, 25);
            this.radioButton_UO.TabIndex = 58;
            this.radioButton_UO.TabStop = true;
            this.radioButton_UO.Text = "Up and Out";
            this.radioButton_UO.UseVisualStyleBackColor = true;
            // 
            // radioButton_UI
            // 
            this.radioButton_UI.AutoSize = true;
            this.radioButton_UI.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_UI.Location = new System.Drawing.Point(22, 114);
            this.radioButton_UI.Name = "radioButton_UI";
            this.radioButton_UI.Size = new System.Drawing.Size(99, 25);
            this.radioButton_UI.TabIndex = 59;
            this.radioButton_UI.TabStop = true;
            this.radioButton_UI.Text = "Up and In";
            this.radioButton_UI.UseVisualStyleBackColor = true;
            // 
            // radioButton_DI
            // 
            this.radioButton_DI.AutoSize = true;
            this.radioButton_DI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_DI.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_DI.Location = new System.Drawing.Point(22, 42);
            this.radioButton_DI.Name = "radioButton_DI";
            this.radioButton_DI.Size = new System.Drawing.Size(124, 25);
            this.radioButton_DI.TabIndex = 56;
            this.radioButton_DI.TabStop = true;
            this.radioButton_DI.Text = "Down and  In";
            this.radioButton_DI.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.radioButton_DI.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.radioButton_DI);
            this.groupBox1.Controls.Add(this.radioButton_UI);
            this.groupBox1.Controls.Add(this.radioButton_DO);
            this.groupBox1.Controls.Add(this.radioButton_UO);
            this.groupBox1.Location = new System.Drawing.Point(189, 492);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 197);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Barrier Option:";
            // 
            // textBox_trials
            // 
            this.textBox_trials.Location = new System.Drawing.Point(243, 314);
            this.textBox_trials.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_trials.Name = "textBox_trials";
            this.textBox_trials.Size = new System.Drawing.Size(124, 27);
            this.textBox_trials.TabIndex = 14;
            this.textBox_trials.TextChanged += new System.EventHandler(this.textBox_trials_TextChanged);
            // 
            // label_trials
            // 
            this.label_trials.AutoSize = true;
            this.label_trials.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_trials.Location = new System.Drawing.Point(166, 322);
            this.label_trials.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_trials.Name = "label_trials";
            this.label_trials.Size = new System.Drawing.Size(57, 19);
            this.label_trials.TabIndex = 13;
            this.label_trials.Text = "Trials:";
            // 
            // label_rebate
            // 
            this.label_rebate.AutoSize = true;
            this.label_rebate.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rebate.Location = new System.Drawing.Point(14, 370);
            this.label_rebate.Name = "label_rebate";
            this.label_rebate.Size = new System.Drawing.Size(209, 19);
            this.label_rebate.TabIndex = 45;
            this.label_rebate.Text = "Rebate for Digital Option:";
            // 
            // label_N
            // 
            this.label_N.AutoSize = true;
            this.label_N.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_N.Location = new System.Drawing.Point(134, 274);
            this.label_N.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_N.Name = "label_N";
            this.label_N.Size = new System.Drawing.Size(89, 19);
            this.label_N.TabIndex = 12;
            this.label_N.Text = "Steps (N):";
            // 
            // textBox_rebate
            // 
            this.textBox_rebate.Location = new System.Drawing.Point(243, 362);
            this.textBox_rebate.Name = "textBox_rebate";
            this.textBox_rebate.Size = new System.Drawing.Size(124, 27);
            this.textBox_rebate.TabIndex = 46;
            // 
            // textBox_N
            // 
            this.textBox_N.Location = new System.Drawing.Point(243, 266);
            this.textBox_N.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_N.Name = "textBox_N";
            this.textBox_N.Size = new System.Drawing.Size(124, 27);
            this.textBox_N.TabIndex = 11;
            this.textBox_N.TextChanged += new System.EventHandler(this.textBox_N_TextChanged);
            // 
            // textBox_sigma
            // 
            this.textBox_sigma.Location = new System.Drawing.Point(243, 218);
            this.textBox_sigma.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_sigma.Name = "textBox_sigma";
            this.textBox_sigma.Size = new System.Drawing.Size(124, 27);
            this.textBox_sigma.TabIndex = 10;
            this.textBox_sigma.TextChanged += new System.EventHandler(this.textBox_sigma_TextChanged);
            // 
            // label_sigma
            // 
            this.label_sigma.AutoSize = true;
            this.label_sigma.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sigma.Location = new System.Drawing.Point(79, 226);
            this.label_sigma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_sigma.Name = "label_sigma";
            this.label_sigma.Size = new System.Drawing.Size(144, 19);
            this.label_sigma.TabIndex = 9;
            this.label_sigma.Text = "Volatility (sigma):";
            // 
            // textBox_T
            // 
            this.textBox_T.Location = new System.Drawing.Point(243, 171);
            this.textBox_T.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_T.Name = "textBox_T";
            this.textBox_T.Size = new System.Drawing.Size(124, 27);
            this.textBox_T.TabIndex = 8;
            this.textBox_T.TextChanged += new System.EventHandler(this.textBox_T_TextChanged);
            // 
            // label_T
            // 
            this.label_T.AutoSize = true;
            this.label_T.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_T.Location = new System.Drawing.Point(136, 178);
            this.label_T.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_T.Name = "label_T";
            this.label_T.Size = new System.Drawing.Size(87, 19);
            this.label_T.TabIndex = 7;
            this.label_T.Text = "Tenor (T):";
            // 
            // textBox_r
            // 
            this.textBox_r.Location = new System.Drawing.Point(243, 122);
            this.textBox_r.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_r.Name = "textBox_r";
            this.textBox_r.Size = new System.Drawing.Size(124, 27);
            this.textBox_r.TabIndex = 6;
            this.textBox_r.TextChanged += new System.EventHandler(this.textBox_r_TextChanged);
            // 
            // label_r
            // 
            this.label_r.AutoSize = true;
            this.label_r.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_r.Location = new System.Drawing.Point(69, 128);
            this.label_r.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_r.Name = "label_r";
            this.label_r.Size = new System.Drawing.Size(154, 19);
            this.label_r.TabIndex = 5;
            this.label_r.Text = "Risk Free Rate (r):";
            // 
            // label_K
            // 
            this.label_K.AutoSize = true;
            this.label_K.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_K.Location = new System.Drawing.Point(88, 78);
            this.label_K.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_K.Name = "label_K";
            this.label_K.Size = new System.Drawing.Size(135, 19);
            this.label_K.TabIndex = 4;
            this.label_K.Text = "Strike Price (K):";
            // 
            // label_barrier
            // 
            this.label_barrier.AutoSize = true;
            this.label_barrier.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_barrier.Location = new System.Drawing.Point(8, 418);
            this.label_barrier.Name = "label_barrier";
            this.label_barrier.Size = new System.Drawing.Size(215, 19);
            this.label_barrier.TabIndex = 54;
            this.label_barrier.Text = "Barrier for Barrier Option:";
            // 
            // textBox_K
            // 
            this.textBox_K.Location = new System.Drawing.Point(243, 75);
            this.textBox_K.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_K.Name = "textBox_K";
            this.textBox_K.Size = new System.Drawing.Size(124, 27);
            this.textBox_K.TabIndex = 2;
            this.textBox_K.TextChanged += new System.EventHandler(this.textBox_K_TextChanged);
            // 
            // textBox_barrier
            // 
            this.textBox_barrier.Location = new System.Drawing.Point(243, 411);
            this.textBox_barrier.Name = "textBox_barrier";
            this.textBox_barrier.Size = new System.Drawing.Size(124, 27);
            this.textBox_barrier.TabIndex = 55;
            // 
            // textBox_S
            // 
            this.textBox_S.Location = new System.Drawing.Point(242, 28);
            this.textBox_S.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_S.Name = "textBox_S";
            this.textBox_S.Size = new System.Drawing.Size(124, 27);
            this.textBox_S.TabIndex = 0;
            this.textBox_S.TextChanged += new System.EventHandler(this.textBox_S_TextChanged);
            // 
            // label_S
            // 
            this.label_S.AutoSize = true;
            this.label_S.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_S.Location = new System.Drawing.Point(50, 30);
            this.label_S.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_S.Name = "label_S";
            this.label_S.Size = new System.Drawing.Size(173, 19);
            this.label_S.TabIndex = 1;
            this.label_S.Text = "Underlying Price (S):";
            this.label_S.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label_S);
            this.groupBox2.Controls.Add(this.label_barrier);
            this.groupBox2.Controls.Add(this.label_K);
            this.groupBox2.Controls.Add(this.label_r);
            this.groupBox2.Controls.Add(this.label_T);
            this.groupBox2.Controls.Add(this.label_sigma);
            this.groupBox2.Controls.Add(this.label_N);
            this.groupBox2.Controls.Add(this.label_rebate);
            this.groupBox2.Controls.Add(this.label_trials);
            this.groupBox2.Controls.Add(this.textBox_S);
            this.groupBox2.Controls.Add(this.textBox_barrier);
            this.groupBox2.Controls.Add(this.textBox_K);
            this.groupBox2.Controls.Add(this.textBox_r);
            this.groupBox2.Controls.Add(this.textBox_T);
            this.groupBox2.Controls.Add(this.textBox_sigma);
            this.groupBox2.Controls.Add(this.textBox_N);
            this.groupBox2.Controls.Add(this.textBox_rebate);
            this.groupBox2.Controls.Add(this.textBox_trials);
            this.groupBox2.Location = new System.Drawing.Point(34, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 462);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 781);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radioButton_RangeOption);
            this.Controls.Add(this.radioButton_LookbackPut);
            this.Controls.Add(this.radioButton_LookbackCall);
            this.Controls.Add(this.radioButton_BarrierPut);
            this.Controls.Add(this.radioButton_BarrierCall);
            this.Controls.Add(this.radioButton_DigitalPut);
            this.Controls.Add(this.radioButton_DigitalCall);
            this.Controls.Add(this.radioButton_AsianPut);
            this.Controls.Add(this.radioButton_AsianCall);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.progressBar_Calculation);
            this.Controls.Add(this.checkBox_MT);
            this.Controls.Add(this.checkBox_CV);
            this.Controls.Add(this.checkBox_Antithetic);
            this.Controls.Add(this.radioButton_EuropeanPut);
            this.Controls.Add(this.radioButton_EuropeanCall);
            this.Controls.Add(this.btn_calculate);
            this.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Optoin Price Calculator 1.0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_r)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_T)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_sigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_trials)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_rebate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider_barrier)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.ErrorProvider errorProvider_S;
        private System.Windows.Forms.ErrorProvider errorProvider_K;
        private System.Windows.Forms.ErrorProvider errorProvider_r;
        private System.Windows.Forms.ErrorProvider errorProvider_T;
        private System.Windows.Forms.ErrorProvider errorProvider_sigma;
        private System.Windows.Forms.ErrorProvider errorProvider_N;
        private System.Windows.Forms.ErrorProvider errorProvider_trials;
        private System.Windows.Forms.RadioButton radioButton_EuropeanPut;
        private System.Windows.Forms.RadioButton radioButton_EuropeanCall;
        private System.Windows.Forms.Label label_OptionResult;
        private System.Windows.Forms.Label label_DeltaResult;
        private System.Windows.Forms.Label label_Delta;
        private System.Windows.Forms.Label label_GammaResult;
        private System.Windows.Forms.Label label_Gamma;
        private System.Windows.Forms.Label label_VegaResult;
        private System.Windows.Forms.Label label_Vega;
        private System.Windows.Forms.Label label_ThetaResult;
        private System.Windows.Forms.Label label_Theta;
        private System.Windows.Forms.Label label_RhoResult;
        private System.Windows.Forms.Label label_Rho;
        private System.Windows.Forms.Label label_SEResult;
        private System.Windows.Forms.Label label_SE;
        private System.Windows.Forms.Label label_TimeResult;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.Label label_CoresResult;
        private System.Windows.Forms.Label label_Cores;
        public System.Windows.Forms.ProgressBar progressBar_Calculation;
        private System.Windows.Forms.Label label_optionPrice;
        public System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.RadioButton radioButton_AsianPut;
        private System.Windows.Forms.RadioButton radioButton_AsianCall;
        private System.Windows.Forms.ErrorProvider errorProvider_rebate;
        private System.Windows.Forms.RadioButton radioButton_RangeOption;
        private System.Windows.Forms.RadioButton radioButton_LookbackPut;
        private System.Windows.Forms.RadioButton radioButton_LookbackCall;
        private System.Windows.Forms.RadioButton radioButton_BarrierPut;
        private System.Windows.Forms.RadioButton radioButton_BarrierCall;
        private System.Windows.Forms.RadioButton radioButton_DigitalPut;
        private System.Windows.Forms.RadioButton radioButton_DigitalCall;
        private System.Windows.Forms.CheckBox checkBox_MT;
        private System.Windows.Forms.CheckBox checkBox_CV;
        private System.Windows.Forms.CheckBox checkBox_Antithetic;
        private System.Windows.Forms.ErrorProvider errorProvider_barrier;
        private System.Windows.Forms.RadioButton radioButton_UI;
        private System.Windows.Forms.RadioButton radioButton_UO;
        private System.Windows.Forms.RadioButton radioButton_DO;
        private System.Windows.Forms.RadioButton radioButton_DI;
        public System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_S;
        private System.Windows.Forms.Label label_barrier;
        private System.Windows.Forms.Label label_K;
        private System.Windows.Forms.Label label_r;
        private System.Windows.Forms.Label label_T;
        private System.Windows.Forms.Label label_sigma;
        private System.Windows.Forms.Label label_N;
        private System.Windows.Forms.Label label_rebate;
        private System.Windows.Forms.Label label_trials;
        private System.Windows.Forms.TextBox textBox_S;
        private System.Windows.Forms.TextBox textBox_barrier;
        private System.Windows.Forms.TextBox textBox_K;
        private System.Windows.Forms.TextBox textBox_r;
        private System.Windows.Forms.TextBox textBox_T;
        private System.Windows.Forms.TextBox textBox_sigma;
        private System.Windows.Forms.TextBox textBox_N;
        private System.Windows.Forms.TextBox textBox_rebate;
        private System.Windows.Forms.TextBox textBox_trials;
    }
}

