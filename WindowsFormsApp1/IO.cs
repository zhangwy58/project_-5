﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public static class IO
    {

        // Using user's inputs
        public static double S0 = 0.0;
        public static double K = 0.0;
        public static double r = 0.0;
        public static double T = 0.0;
        public static double sigma = 0.0;
        public static int N = 0;
        public static int trials = 0;
        public static double rebate = 0;
        public static double barrier = 0;
        public static int numOfThread = 1;
        public static bool isSE = false;
        public static bool isAnti = false;
        public static bool isput = false;
        public static bool useCV = false;
        public static bool KnockIn = false;



        // My final outputs variables
        public static double myEuroPrice { get; set; }
        public static double myEuroDelta { get; set; }
        public static double myEuroGamma { get; set; }
        public static double myEuroVega { get; set; }
        public static double myEuroTheta { get; set; }
        public static double myEuroRho { get; set; }
        public static double myEuroSE { get; set; }
        public static string myTime { get; set; }

        public static double myAsianPrice { get; set; }
        public static double myAsianDelta { get; set; }
        public static double myAsianGamma { get; set; }
        public static double myAsianVega { get; set; }
        public static double myAsianTheta { get; set; }
        public static double myAsianRho { get; set; }
        public static double myAsianSE { get; set; }

        public static double myDigitalPrice { get; set; }
        public static double myDigitalDelta { get; set; }
        public static double myDigitalGamma { get; set; }
        public static double myDigitalVega { get; set; }
        public static double myDigitalTheta { get; set; }
        public static double myDigitalRho { get; set; }
        public static double myDigitalSE { get; set; }

        public static double myBarrierPrice { get; set; }
        public static double myBarrierDelta { get; set; }
        public static double myBarrierGamma { get; set; }
        public static double myBarrierVega { get; set; }
        public static double myBarrierTheta { get; set; }
        public static double myBarrierRho { get; set; }
        public static double myBarrierSE { get; set; }

        public static double myLookbackPrice { get; set; }
        public static double myLookbackDelta { get; set; }
        public static double myLookbackGamma { get; set; }
        public static double myLookbackVega { get; set; }
        public static double myLookbackTheta { get; set; }
        public static double myLookbackRho { get; set; }
        public static double myLookbackSE { get; set; }

        public static double myRangePrice { get; set; }
        public static double myRangeDelta { get; set; }
        public static double myRangeGamma { get; set; }
        public static double myRangeVega { get; set; }
        public static double myRangeTheta { get; set; }
        public static double myRangeRho { get; set; }
        public static double myRangeSE { get; set; }
    }
}