﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Summary
    {
        public void Sum()
        {
            // if all the inputs are correct, do the following calculation
            // Calculating the European call option price and greeks
            EuroOption opt1 = new EuroOption();
            Greeks greek1 = new Greeks();

            // Create a instance for RandomGenerator class
            RandomGenerator RG = new RandomGenerator();

            double[,] RandomNormal1 = RG.RandomNormal(IO.trials, IO.N, IO.numOfThread);
            double[,] RandomNormal2 = RG.NegtRandomNormal(IO.trials, IO.N, RandomNormal1, IO.numOfThread);

            // Calculating and outputing the european call option price
            IO.myEuroPrice = opt1.EUPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);
            // Calculating and outputing the delta value for European call option when user want to use antithetic method to reduce variance

            IO.myEuroDelta = greek1.Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

            // Calculating and outputing the gamma value for European call option when user want to use antithetic method to reduce variance
            IO.myEuroGamma = greek1.Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            

            // Calculating and outputing the vega value for European call option when user want to use antithetic method to reduce variance
            IO.myEuroVega = greek1.Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            

            // Calculating and outputing the theta value for European call option when user want to use antithetic method to reduce variance
            IO.myEuroTheta = greek1.Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            

            // Calculating and outputing the rho value for European call option when user want to use antithetic method to reduce variance
            IO.myEuroRho = greek1.Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            

            // Calculating and outputing the SE value for European call option when user want to use antithetic method to reduce variance
            IO.myEuroSE = greek1.SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            

            //
            IO.myAsianPrice = opt1.AsianPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

            // 
            IO.myAsianDelta = greek1.AsianOption_Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myAsianGamma = greek1.AsianOption_Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            
            //
            IO.myAsianVega = greek1.AsianOption_Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            
            //
            IO.myAsianTheta = greek1.AsianOption_Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            
            //
            IO.myAsianRho = greek1.AsianOption_Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            
            //
            IO.myAsianSE = greek1.AsianOption_SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalPrice = opt1.DigitalPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

            //
            IO.myDigitalDelta = greek1.DigitalOption_Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalGamma = greek1.DigitalOption_Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalVega = greek1.DigitalOption_Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalTheta = greek1.DigitalOption_Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalRho = greek1.DigitalOption_Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myDigitalSE = greek1.DigitalOption_SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.rebate, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            
            //
            IO.myBarrierPrice = opt1.BarrierPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);


            //
            IO.myBarrierDelta = greek1.BarrierOption_Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);
            
            //
            IO.myBarrierGamma = greek1.BarrierOption_Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);

            //
            IO.myBarrierVega = greek1.BarrierOption_Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);

            //
            IO.myBarrierTheta = greek1.BarrierOption_Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);

            //
            IO.myBarrierRho = greek1.BarrierOption_Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);

            //
            IO.myBarrierSE = greek1.BarrierOption_SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, IO.barrier, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread, IO.KnockIn);
            
            //
            IO.myLookbackPrice = opt1.lookBackPrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

            //
            IO.myLookbackDelta = greek1.LookbackOption_Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myLookbackGamma = greek1.LookbackOption_Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myLookbackVega = greek1.LookbackOption_Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myLookbackTheta = greek1.LookbackOption_Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myLookbackRho = greek1.LookbackOption_Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            // 
            IO.myLookbackSE = greek1.LookbackOption_SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);



            //
            IO.myRangePrice = opt1.RangePrice(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);

            //
            IO.myRangeDelta = greek1.RangeOption_Delta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myRangeGamma = greek1.RangeOption_Gamma(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myRangeVega = greek1.RangeOption_Vega(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            // 
            IO.myRangeTheta = greek1.RangeOption_Theta(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myRangeRho = greek1.RangeOption_Rho(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);

            //
            IO.myRangeSE = greek1.RangeOption_SE(IO.S0, IO.K, IO.r, IO.T, IO.sigma, IO.trials, IO.N, RandomNormal1, RandomNormal2, IO.isput, IO.isAnti, IO.isSE, IO.useCV, IO.numOfThread);
            Program.increase(1);












        }
    }
}
