﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

/// <summary>
/// This class is about GUI, inputs and outputs
/// </summary>
namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        // The error providers for inputs.
        // When user input a wrong type of data, the program should ask user to enter a right one
        private void Form1_Load(object sender, EventArgs e)
        {
            // Set some default value for convenient
            textBox_S.Text = "50";
            textBox_K.Text = "50";
            textBox_r.Text = "0.05";
            textBox_T.Text = "1";
            textBox_sigma.Text = "0.5";
            textBox_N.Text = "100";
            textBox_trials.Text = "10000";
            textBox_rebate.Text = "5";
            textBox_barrier.Text = "40";

            // Check how many cores in this computer
            int numOfThread = System.Environment.ProcessorCount;
            label_CoresResult.Text = Convert.ToString(numOfThread);

            // The error provider for underlying price text box
            errorProvider_S.SetIconAlignment(textBox_S, ErrorIconAlignment.MiddleRight);
            errorProvider_S.SetIconPadding(textBox_S, 3);

            // The error provider for strike price text box
            errorProvider_K.SetIconAlignment(textBox_K, ErrorIconAlignment.MiddleRight);
            errorProvider_K.SetIconPadding(textBox_K, 3);

            // The error provider for risk free rate text box
            errorProvider_r.SetIconAlignment(textBox_r, ErrorIconAlignment.MiddleRight);
            errorProvider_r.SetIconPadding(textBox_r, 3);

            // The error provider for tenor text box
            errorProvider_T.SetIconAlignment(textBox_T, ErrorIconAlignment.MiddleRight);
            errorProvider_T.SetIconPadding(textBox_T, 3);

            // The error provider for volatility text box
            errorProvider_sigma.SetIconAlignment(textBox_sigma, ErrorIconAlignment.MiddleRight);
            errorProvider_sigma.SetIconPadding(textBox_sigma, 3);

            // The error provider for steps text box
            errorProvider_N.SetIconAlignment(textBox_N, ErrorIconAlignment.MiddleRight);
            errorProvider_N.SetIconPadding(textBox_N, 3);

            // The error provider for trials text box
            errorProvider_trials.SetIconAlignment(textBox_trials, ErrorIconAlignment.MiddleRight);
            errorProvider_trials.SetIconPadding(textBox_trials, 3);

            // The error provider for rebate text box
            errorProvider_rebate.SetIconAlignment(textBox_rebate, ErrorIconAlignment.MiddleRight);
            errorProvider_rebate.SetIconPadding(textBox_rebate, 3);

            // The error provider for barrier text box
            errorProvider_barrier.SetIconAlignment(textBox_barrier, ErrorIconAlignment.MiddleRight);
            errorProvider_barrier.SetIconPadding(textBox_barrier, 3);

            // Clear the progress bar
            progressBar_Calculation.Value = 0;

            /*
            if (IO.S0 > IO.barrier)
            {
                radioButton_UI.Enabled = false;
                radioButton_UO.Enabled = false;
            }
            else if (IO.S0 < IO.barrier)
            {
                radioButton_DI.Enabled = false;
                radioButton_DO.Enabled = false;
            }
            */
        }

        // The sentence that ask user to enter correct type of data for underlying price
        private void textBox_S_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_S.Text, out num))
            {
                errorProvider_S.SetError(textBox_S, "Please enter a number");
            }
            else
            {
                errorProvider_S.SetError(textBox_S, "");
            }

        }

        // The sentence that ask user to enter correct type of data for strike price
        private void textBox_K_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_K.Text, out num))
            {
                errorProvider_K.SetError(textBox_K, "Please enter a number");
            }
            else
            {
                errorProvider_K.SetError(textBox_K, "");
            }
        }

        // The sentence that ask user to enter correct type of data for risk free rate
        private void textBox_r_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_r.Text, out num))
            {
                errorProvider_r.SetError(textBox_r, "Please enter a number");
            }
            else
            {
                errorProvider_r.SetError(textBox_r, "");
            }
        }

        // The sentence that ask user to enter correct type of data for tenor
        private void textBox_T_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_r.Text, out num))
            {
                errorProvider_T.SetError(textBox_T, "Please enter a number");
            }
            else
            {
                errorProvider_T.SetError(textBox_T, "");
            }
        }

        // The sentence that ask user to enter correct type of data for volatility
        private void textBox_sigma_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_sigma.Text, out num))
            {
                errorProvider_sigma.SetError(textBox_sigma, "Please enter a number");
            }
            else
            {
                errorProvider_sigma.SetError(textBox_sigma, "");
            }
        }

        // The sentence that ask user to enter correct type of data for steps
        private void textBox_N_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_N.Text, out num))
            {
                errorProvider_N.SetError(textBox_N, "Please enter a number");
            }
            else
            {
                errorProvider_N.SetError(textBox_N, "");
            }
        }

        // The sentence that ask user to enter correct type of data for trials
        private void textBox_trials_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_trials.Text, out num))
            {
                errorProvider_trials.SetError(textBox_trials, "Please enter a number");
            }
            else
            {
                errorProvider_trials.SetError(textBox_trials, "");
            }
        }

        // The sentence that ask user to enter correct type of data for rebate
        private void textBox_rebate_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_rebate.Text, out num))
            {
                errorProvider_rebate.SetError(textBox_rebate, "Please enter a number");
            }
            else
            {
                errorProvider_rebate.SetError(textBox_rebate, "");
            }
        }

        // The sentence that ask user to enter correct type of data for barrier
        private void textBox_barrier_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(textBox_barrier.Text, out num))
            {
                errorProvider_barrier.SetError(textBox_barrier, "Please enter a number");
            }
            else
            {
                errorProvider_barrier.SetError(textBox_barrier, "");
            }
        }

        private void btn_calculate_Click(object sender, EventArgs e)
        {

            // Clear the progress bar
            progressBar_Calculation.Value = 0;
            EuroOption opt = new EuroOption();

            if (double.TryParse(textBox_S.Text, out IO.S0) &&
                double.TryParse(textBox_K.Text, out IO.K) &&
                double.TryParse(textBox_r.Text, out IO.r) &&
                double.TryParse(textBox_T.Text, out IO.T) &&
                double.TryParse(textBox_sigma.Text, out IO.sigma) &&
                int.TryParse(textBox_N.Text, out IO.N) &&
                int.TryParse(textBox_trials.Text, out IO.trials) &&
                double.TryParse(textBox_rebate.Text, out IO.rebate)&&
                double.TryParse(textBox_barrier.Text, out IO.barrier))
            {

                // Check whether user want to calculate European call or put option
                if (radioButton_EuropeanCall.Checked)
                {
                    IO.isput = false;
                }
                else if (radioButton_EuropeanPut.Checked)
                {
                    IO.isput = true;
                }
                else if (radioButton_AsianCall.Checked)
                {
                    IO.isput = false;
                }
                else if (radioButton_AsianPut.Checked)
                {
                    IO.isput = true;
                }
                else if (radioButton_DigitalCall.Checked)
                {
                    IO.isput = false;
                }
                else if (radioButton_DigitalPut.Checked)
                {
                    IO.isput = true;
                }
                else if (radioButton_BarrierCall.Checked)
                {
                    IO.isput = false;
                }
                else if (radioButton_BarrierPut.Checked)
                {
                    IO.isput = true;
                }
                else if (radioButton_LookbackCall.Checked)
                {
                    IO.isput = false;
                }
                else if (radioButton_LookbackPut.Checked)
                {
                    IO.isput = true;
                }

                if (radioButton_DI.Checked || radioButton_UI.Checked)
                {
                    IO.KnockIn = true;
                }
                else if (radioButton_DO.Checked || radioButton_UO.Checked)
                {
                    IO.KnockIn = false;
                }

                // Check whether user want to use control variate to reduce variance or not
                if (checkBox_CV.Checked)
                {
                    IO.useCV = true;
                }
                else
                {
                    IO.useCV = false;
                }

                // Check whether user want to use antithetic method or not
                if (checkBox_Antithetic.Checked)
                {
                    IO.isAnti = true;
                }
                else
                {
                    IO.isAnti = false;
                }

                progressBar_Calculation.Value = 0;

                // Check whether user want to use multi-threading
                if (checkBox_MT.Checked)
                {
                    IO.numOfThread = System.Environment.ProcessorCount;
                }
                else
                {
                    IO.numOfThread = System.Environment.ProcessorCount;
                }

                Thread C = new Thread(new ThreadStart(opt.scheduler));
                C.Start();
            }
            // Output the error message if the user inputs inpropriate data
            else
            {
                label_OptionResult.Text = "Error! Please check your inputs";
            }

        }

        // The function used to output the result
        public void finish()
        {
            if (radioButton_EuropeanPut.Checked || radioButton_EuropeanCall.Checked)
            {
                label_OptionResult.Text = IO.myEuroPrice.ToString();
                label_DeltaResult.Text = IO.myEuroDelta.ToString();
                label_GammaResult.Text = IO.myEuroGamma.ToString();
                label_VegaResult.Text = IO.myEuroVega.ToString();
                label_ThetaResult.Text = IO.myEuroTheta.ToString();
                label_RhoResult.Text = IO.myEuroRho.ToString();
                label_SEResult.Text = IO.myEuroSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_AsianPut.Checked || radioButton_AsianCall.Checked)
            {
                label_OptionResult.Text = IO.myAsianPrice.ToString();
                label_DeltaResult.Text = IO.myAsianDelta.ToString();
                label_GammaResult.Text = IO.myAsianGamma.ToString();
                label_VegaResult.Text = IO.myAsianVega.ToString();
                label_ThetaResult.Text = IO.myAsianTheta.ToString();
                label_RhoResult.Text = IO.myAsianRho.ToString();
                label_SEResult.Text = IO.myAsianSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_DigitalPut.Checked || radioButton_DigitalCall.Checked)
            {
                label_OptionResult.Text = IO.myDigitalPrice.ToString();
                label_DeltaResult.Text = IO.myDigitalDelta.ToString();
                label_GammaResult.Text = IO.myDigitalGamma.ToString();
                label_VegaResult.Text = IO.myDigitalVega.ToString();
                label_ThetaResult.Text = IO.myDigitalTheta.ToString();
                label_RhoResult.Text = IO.myDigitalRho.ToString();
                label_SEResult.Text = IO.myDigitalSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_BarrierPut.Checked || radioButton_BarrierCall.Checked)
            {
                label_OptionResult.Text = IO.myBarrierPrice.ToString();
                label_DeltaResult.Text = IO.myBarrierDelta.ToString();
                label_GammaResult.Text = IO.myBarrierGamma.ToString();
                label_VegaResult.Text = IO.myBarrierVega.ToString();
                label_ThetaResult.Text = IO.myBarrierTheta.ToString();
                label_RhoResult.Text = IO.myBarrierRho.ToString();
                label_SEResult.Text = IO.myBarrierSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_BarrierPut.Checked || radioButton_BarrierCall.Checked)
            {
                label_OptionResult.Text = IO.myBarrierPrice.ToString();
                label_DeltaResult.Text = IO.myBarrierDelta.ToString();
                label_GammaResult.Text = IO.myBarrierGamma.ToString();
                label_VegaResult.Text = IO.myBarrierVega.ToString();
                label_ThetaResult.Text = IO.myBarrierTheta.ToString();
                label_RhoResult.Text = IO.myBarrierRho.ToString();
                label_SEResult.Text = IO.myBarrierSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_LookbackPut.Checked || radioButton_LookbackCall.Checked)
            {
                label_OptionResult.Text = IO.myLookbackPrice.ToString();
                label_DeltaResult.Text = IO.myLookbackDelta.ToString();
                label_GammaResult.Text = IO.myLookbackGamma.ToString();
                label_VegaResult.Text = IO.myLookbackVega.ToString();
                label_ThetaResult.Text = IO.myLookbackTheta.ToString();
                label_RhoResult.Text = IO.myLookbackRho.ToString();
                label_SEResult.Text = IO.myLookbackSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_RangeOption.Checked)
            {
                label_OptionResult.Text = IO.myRangePrice.ToString();
                label_DeltaResult.Text = IO.myRangeDelta.ToString();
                label_GammaResult.Text = IO.myRangeGamma.ToString();
                label_VegaResult.Text = IO.myRangeVega.ToString();
                label_ThetaResult.Text = IO.myRangeTheta.ToString();
                label_RhoResult.Text = IO.myRangeRho.ToString();
                label_SEResult.Text = IO.myRangeSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
            else if (radioButton_BarrierCall.Checked || radioButton_BarrierPut.Checked)
            {
                label_OptionResult.Text = IO.myBarrierPrice.ToString();
                label_DeltaResult.Text = IO.myBarrierDelta.ToString();
                label_GammaResult.Text = IO.myBarrierGamma.ToString();
                label_VegaResult.Text = IO.myBarrierVega.ToString();
                label_ThetaResult.Text = IO.myBarrierTheta.ToString();
                label_RhoResult.Text = IO.myBarrierRho.ToString();
                label_SEResult.Text = IO.myBarrierSE.ToString();
                label_CoresResult.Text = IO.numOfThread.ToString();
                label_TimeResult.Text = IO.myTime.ToString();
            }
        }
    }
}
