﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This class is used to calculate the greeks and standard error
/// </summary>
namespace WindowsFormsApp1
{
    class Greeks
    {
        Option opt1 = new Option();
        EuroOption opt2 = new EuroOption();


        // The function used to calculate the European Option's Delta
        public double Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {

                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }

        }

        // The function used to calculate the European Option's Gamma 
        public double Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;

                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }


        }

        // The function used to calculate the European Option's Vega 
        public double Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.EUPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.EUPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }

        }

        // The function used to calculate the European Option's Theta 
        public double Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.EUPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }


        }

        // The function used to calculate the European Option's rho  
        public double Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.EUPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.EUPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
        }

        // The function to calculate European Option's standard error value 
        public double SE(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(St1 - K, 0) + beta1 * cv1) + (Math.Max(St2 - K, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                    else
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(K - St1, 0) + beta1 * cv1) + (Math.Max(K - St2, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                }
                else
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;


                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(St - K, 0) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                    else
                    {

                        for (int i = 0; i < trials; i++)
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(0, K - St) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {

                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                    double EUPrice = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);


                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow(((Math.Max(sims[a, N] - K, 0) * Math.Exp(-r * T) + Math.Max(sims2[a, N] - K, 0)) / 2 - EUPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow(((Math.Max(K - sims[a, N], 0) * Math.Exp(-r * T) + Math.Max(K - sims2[a, N], 0)) / 2 - EUPrice), 2);
                        }

                        // sums = sums + Math.Pow(((sims[a, N] * Math.Exp(-r * T) + sims2[a, N] * Math.Exp(-r * T)) / 2 - EUPrice), 2);
                    }

                    double SD = Math.Sqrt(sums / (trials - 1));

                    return SD / Math.Sqrt(trials);
                }
                else
                {
                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double EUPrice = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow((Math.Max(sims[a, N] - K, 0) * Math.Exp(-r * T) - EUPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow((Math.Max(K - sims[a, N], 0) * Math.Exp(-r * T) - EUPrice), 2);
                        }

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }
            }
        }

        // The function to calculate Asian option's Delta
        public double AsianOption_Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {

                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }

        }

        // The function to calculate Asian option's Gamma
        public double AsianOption_Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;

                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
        }

        // The function to calculate Asian option's Vega
        public double AsianOption_Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.AsianPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.AsianPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }

        }

        // The function to calculate Asian option's Theta
        public double AsianOption_Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.AsianPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
        }

        // The function to calculate Asian option's rho
        public double AsianOption_Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.AsianPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.AsianPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
        }

        // The function to calculate Asian option's standard error value
        public double AsianOption_SE(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            ControlVariate CV1 = new ControlVariate();
            EuroOption opt1 = new EuroOption();

            if (useCV == true)
            {
                Simulator sims = new Simulator();
                double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                double dt = T / N;

                double nudt = (r - 0.5 * sigma * sigma) * dt;

                double sigsdt = sigma * Math.Sqrt(dt);

                double erddt = Math.Exp(r * dt);

                double beta1 = -1;

                double sum_CT = 0;
                double sum_CT2 = 0;
                double[] sum_CT_array = new double[trials];
                double[] sum_CT2_array = new double[trials];

                // 
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Getting the average of St1 and St2
                        double St_sum1 = 0.0;
                        double St_sum2 = 0.0;

                        double[] St_average1 = new double[trials];
                        double[] St_average2 = new double[trials];

                        for (int i = 0; i < trials; i++)
                        {
                            St_sum1 = 0;
                            St_sum2 = 0;
                            for (int j = 0; j < N; j++)
                            {
                                St_sum1 = St_sum1 + sim1[i, j];
                                St_sum2 = St_sum2 + sim2[i, j];
                            }
                            St_average1[i] = St_sum1 / N;
                            St_average2[i] = St_sum2 / N;
                        }

                        // Using parallel to calculate
                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(St_average1[i] - K, 0) + beta1 * cv1) + (Math.Max(St_average2[i] - K, 0) + beta1 * cv2));

                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;
                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;

                    }
                    else
                    {
                        // Getting the average of St1 and St2
                        double St_sum1 = 0.0;
                        double St_sum2 = 0.0;

                        double[] St_average1 = new double[trials];
                        double[] St_average2 = new double[trials];

                        for (int i = 0; i < trials; i++)
                        {
                            St_sum1 = 0;
                            St_sum2 = 0;
                            for (int j = 0; j < N; j++)
                            {
                                St_sum1 = St_sum1 + sim1[i, j];
                                St_sum2 = St_sum2 + sim2[i, j];
                            }
                            St_average1[i] = St_sum1 / N;
                            St_average2[i] = St_sum2 / N;
                        }

                        //
                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(K - St_average1[i], 0) + beta1 * cv1) + (Math.Max(K - St_average2[i], 0) + beta1 * cv2));
                            // sum_CT = sum_CT + CT;
                            // sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;
                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;

                    }
                }
                else
                {
                    if (isput == false)
                    {

                        double St_sum = 0.0;
                        double[] St_average = new double[trials];

                        for (int i = 0; i < trials; i++)
                        {
                            St_sum = 0;
                            for (int j = 0; j < N; j++)
                            {
                                St_sum = St_sum + sim1[i, j];
                            }
                            St_average[i] = St_sum / N;
                        }

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(St_average[i] - K, 0) + beta1 * cv;
                            // sum_CT = sum_CT + CT;
                            // sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;

                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);
                        return SE;


                    }
                    else
                    {
                        double St_sum = 0.0;
                        double[] St_average = new double[trials];

                        for (int i = 0; i < trials; i++)
                        {
                            St_sum = 0;
                            for (int j = 0; j < N; j++)
                            {
                                St_sum = St_sum + sim1[i, j];
                            }
                            St_average[i] = St_sum / N;
                        }

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(0, K - St_average[i]) + beta1 * cv;
                            // sum_CT = sum_CT + CT;
                            // sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;

                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {

                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sim1 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                    double AsianPrice = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                    double St_sum1 = 0.0;
                    double St_sum2 = 0.0;

                    double[] St_average1 = new double[trials];
                    double[] St_average2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        St_sum1 = 0;
                        St_sum2 = 0;
                        for (int j = 0; j < N; j++)
                        {
                            St_sum1 = St_sum1 + sim1[i, j];
                            St_sum2 = St_sum2 + sim2[i, j];
                        }
                        St_average1[i] = St_sum1 / N;
                        St_average2[i] = St_sum2 / N;
                    }

                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow(((Math.Max(St_average1[i] - K, 0) * Math.Exp(-r * T) + Math.Max(St_average2[i] - K, 0)) / 2 - AsianPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow(((Math.Max(K - St_average1[i], 0) * Math.Exp(-r * T) + Math.Max(K - St_average2[i], 0)) / 2 - AsianPrice), 2);
                        }

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));

                    return SD / Math.Sqrt(trials);
                }
                else
                {
                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double AsianPrice = opt2.AsianPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                    double St_sum1 = 0.0;                   

                    double[] St_average1 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        St_sum1 = 0;

                        for (int j = 0; j < N; j++)
                        {
                            St_sum1 = St_sum1 + sims[i, j];
                        }
                        St_average1[i] = St_sum1 / N;
                    }

                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            sums = sums + Math.Pow((Math.Max(St_average1[i] - K, 0) * Math.Exp(-r * T) - AsianPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow((Math.Max(K - St_average1[i], 0) * Math.Exp(-r * T) - AsianPrice), 2);
                        }

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }
            }

        }

        
        // The function to calculate Delta
        public double DigitalOption_Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);
                        double Delta_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {

                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.AsianPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.AsianPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
        }

        //The function to calculate Gamma
        public double DigitalOption_Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;

                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.DigitalPrice(S0a, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.DigitalPrice(S0m, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
        }

        // The function to calculate Vega
        public double DigitalOption_Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.DigitalPrice(S0, K, r, T, sigmaa, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.DigitalPrice(S0, K, r, T, sigmam, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
        }

        // The function to calculate Theta
        public double DigitalOption_Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.DigitalPrice(S0, K, r, Tm, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
        }

        // The function to calculate Rho
        public double DigitalOption_Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.DigitalPrice(S0, K, ra, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.DigitalPrice(S0, K, rm, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
        }

        // The function to calculate SE
        public double DigitalOption_SE(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            ControlVariate CV1 = new ControlVariate();
            EuroOption opt1 = new EuroOption();

            Simulator sims = new Simulator();
            double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
            double DigitalPrice = opt1.DigitalPrice(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;

            double sum1 = 0;
            double sum2 = 0;

            double[] lastPrice1 = new double[trials];
            double[] lastPrice2 = new double[trials];

            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];

            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        double sum_rebate1 = 0;
                        double sum_rebate2 = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice1[i] = sim1[i, N];
                            if (lastPrice1[i] < K)
                            {
                                sum_rebate1 = sum_rebate1 + 0;
                            }
                            else
                            {
                                sum_rebate1 = sum_rebate1 + rebate;
                            }

                        }
                        double p1 = sum_rebate1 / trials;

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice2[i] = sim2[i, N];
                            if (lastPrice2[i] < K)
                            {
                                sum_rebate2 = sum_rebate2 + 0;
                            }
                            else
                            {
                                sum_rebate2 = sum_rebate2 + rebate;
                            }
                        }
                        double p2 = sum_rebate2 / trials;

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((p1 + beta1 * cv1) + (p2 + beta1 * cv2));


                            //sum_CT = sum_CT + CT;
                            //sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;
                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                    else
                    {
                        double sum_rebate1 = 0;
                        double sum_rebate2 = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice1[i] = sim1[i, N];
                            if (lastPrice1[i] > K)
                            {
                                sum_rebate1 = sum_rebate1 + 0;
                            }
                            else
                            {
                                sum_rebate1 = sum_rebate1 + rebate;
                            }

                        }
                        double p1 = sum_rebate1 / trials;

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice2[i] = sim2[i, N];
                            if (lastPrice2[i] > K)
                            {
                                sum_rebate2 = sum_rebate2 + 0;
                            }
                            else
                            {
                                sum_rebate2 = sum_rebate2 + rebate;
                            }

                        }
                        double p2 = sum_rebate2 / trials;

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((p1 + beta1 * cv1) + (p2 + beta1 * cv2));


                            //sum_CT = sum_CT + CT;
                            //sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;
                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        double sum_rebate = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice1[i] = sim1[i, N];

                            if (lastPrice1[i] < K)
                            {
                                sum_rebate = sum_rebate + 0;
                            }
                            else
                            {
                                sum_rebate = sum_rebate + rebate;
                            }

                        }
                        double p = sum_rebate / trials;

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = p + beta1 * cv;
                            // sum_CT = sum_CT + CT;
                            // sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;

                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                    else
                    {
                        double sum_rebate = 0;
                        for (int i = 0; i < trials; i++)
                        {

                            lastPrice1[i] = sim1[i, N];

                            if (lastPrice1[i] > K)
                            {
                                sum_rebate = sum_rebate + 0;
                            }
                            else
                            {
                                sum_rebate = sum_rebate + rebate;
                            }

                        }
                        double p = sum_rebate / trials;

                        Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                        {
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = p + beta1 * cv;
                            // sum_CT = sum_CT + CT;
                            // sum_CT2 = sum_CT2 + CT * CT;
                            sum_CT_array[i] = CT;
                            sum_CT2_array[i] = CT * CT;

                        });
                        for (int i = 0; i < trials; i++)
                        {
                            sum_CT = sum_CT + sum_CT_array[i];
                            sum_CT2 = sum_CT2 + sum_CT2_array[i];
                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            double a = 0;
                            double b = 0;
                            if (sim1[i, N] > K)
                            {
                                a = rebate;
                            }
                            if (sim2[i, N] > K)
                            {
                                b = rebate;
                            }
                            
                                sum1 = sum1 + Math.Pow(((Math.Max(a, 0) * Math.Exp(-r * T) + Math.Max(b, 0) * Math.Exp(-r * T)) / 2 - DigitalPrice), 2);
                            
                        }
                        else
                        {
                            double a = 0;
                            double b = 0;
                            if (sim1[i, N] < K)
                            {
                                a = rebate;
                            }
                            if (sim2[i, N] < K)
                            {
                                b = rebate;
                            }
                            sum1 = sum1 + Math.Pow(((Math.Max(a, 0) * Math.Exp(-r * T) + Math.Max(b, 0) * Math.Exp(-r * T)) / 2 - DigitalPrice), 2);
                            
                        }
                    }
                    double SD = Math.Sqrt(sum1 / (trials - 1));
                    return SD / Math.Sqrt(trials);
                    
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            double a = 0;
                            if (sim1[i, N] > K)
                            {
                                a = rebate;
                            }
                            sum1 = sum1 + Math.Pow((Math.Max(a, 0) * Math.Exp(-r * T) - DigitalPrice), 2);
                            
                        }
                        else
                        {
                            double a = 0;
                            if (sim1[i, N] < K)
                            {
                                a = 0;
                            }
                           sum1 = sum1 + Math.Pow((Math.Max(a, 0) * Math.Exp(-r * T) - DigitalPrice), 2);
                           
                        }
                    }
                    double SD = Math.Sqrt(sum1 / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }

            }
        }



        // The function to calculate Delta
        public double BarrierOption_Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {

            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {

                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                            return Delta;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                            return Delta;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                            return Delta;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                            return Delta;
                        }
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the delta value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;


                            // Using the new simulation to calculate the option price
                            double Delta_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Delta_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculating the delta value
                            double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                            return Delta_Antithetic;
                        }
                    }

                }
            }
        }
        
        //The function to calculate Gamma
        public double BarrierOption_Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (KnockIn == true)
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }
                    else
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }


                }
                else
                {
                    if (KnockIn == true)
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }
                    else
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (KnockIn == true)
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }
                    else
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }

                }
                else
                {
                    if (KnockIn == true)
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }
                    else
                    {
                        if (isput == false)
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.BarrierPrice(S0a, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p2 = opt2.BarrierPrice(S0m, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Gamma_p3 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the gamma value
                            double dS0 = S0 * 0.001;
                            double S0a = S0 + dS0;
                            double S0m = S0 - dS0;

                            // Using the new simulation to calculate the option price
                            double Gamma_p1 = opt2.EUPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p2 = opt2.EUPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                            double Gamma_p3 = opt2.EUPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                            // Calculate the gamma value
                            double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                            return Gamma_Antithetic;
                        }
                    }

                }
            }
        }

        // The function to calculate Vega
        public double BarrierOption_Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the vega value
                            double dsigma = sigma * 0.001;
                            double sigmaa = sigma + dsigma;
                            double sigmam = sigma - dsigma;

                            // Using the new simulation to calculate the option price
                            double Vega_p1 = opt2.BarrierPrice(S0, K, r, T, sigmaa, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Vega_p2 = opt2.BarrierPrice(S0, K, r, T, sigmam, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the vega value
                            double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                            return Vega_Antithetic;
                        }
                    }
                }
            }
        }

        // The function to calculate Theta
        public double BarrierOption_Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the theta value
                            double dT = T * 0.001;
                            double Tm = T - dT;

                            // Using the new simulation to calculate the option price
                            double Theta_p1 = opt2.BarrierPrice(S0, K, r, Tm, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Theta_p2 = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                            return Theta_Antithetic;
                        }
                    }
                }
            }
        }

        // The function to calculate Rho
        public double BarrierOption_Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == true)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }

                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                }
                else
                {
                    if (isput == true)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == true)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                }
                else
                {
                    if (isput == true)
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                    else
                    {
                        if (KnockIn == true)
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                        else
                        {
                            // Define the variables for calculating the rho value
                            double dr = r * 0.001;
                            double ra = r + dr;
                            double rm = r - dr;

                            // Using the new simulation to calculate the option price
                            double Rho_p1 = opt2.BarrierPrice(S0, K, ra, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                            double Rho_p2 = opt2.BarrierPrice(S0, K, rm, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);

                            // Calculate the theta value
                            double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                            return Rho_Antithetic;
                        }
                    }
                }
            }
        }

        // The function to calculate SE
        public double BarrierOption_SE(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            Simulator sim = new Simulator();

            if (KnockIn == true)
            {
                Simulator sims = new Simulator();
                
                double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                double BarrierPrice = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                double sum = 0;

                double[,] A1 = new double[trials, N + 1];
                double[] A_trials1 = new double[trials];

                double[,] A2 = new double[trials, N + 1];
                double[] A_trials2 = new double[trials];

                double dt = T / N;

                double nudt = (r - 0.5 * sigma * sigma) * dt;

                double sigsdt = sigma * Math.Sqrt(dt);

                double erddt = Math.Exp(r * dt);

                double beta1 = -1;

                double sum_CT = 0;
                double sum_CT2 = 0;
                double[] sum_CT_array = new double[trials];
                double[] sum_CT2_array = new double[trials];

                double p1 = 0;
                double SE = 0;

                double a, b;

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] < barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] > barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] < barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] > barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }

                if (useCV == true && isAnti == true)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St2 - K) + beta1 * cv2);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St1, 0) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St2, 0) + beta1 * cv2);
                            }
                        }

                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    SE = SD / Math.Sqrt(trials);

                }
                else if (useCV == true && isAnti == false)
                {

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {

                        double St = S0;
                        double cv = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = Math.Max(St - K, 0) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = Math.Max(K - St, 0) + beta1 * cv;
                            }
                        }
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    SE = SD / Math.Sqrt(trials);
                }
                else if (useCV == false && isAnti == true)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        a = 0;
                        b = 0;
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                a = sim1[i, N] - K;
                            }
                            if (A_trials2[i] == 0)
                            {
                                b = sim2[i, N] - K;
                            }
                            sum = sum + Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - BarrierPrice), 2);
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                a = -sim1[i, N] + K;
                            }
                            if (A_trials2[i] == 0)
                            {
                                b = -sim2[i, N] + K;
                            }
                            sum = sum + Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - BarrierPrice), 2);
                        }
                    }
                    p1 = (sum / trials / 2) * Math.Exp(-r * T);
                    SE = Math.Sqrt(sum / (trials - 1) / trials);
                }
                else if (useCV == false && isAnti == false)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Pow((Math.Max(sim1[i, N] - K, 0) * Math.Exp(-r * T) - BarrierPrice), 2);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Pow((Math.Max(K - sim1[i, N], 0) * Math.Exp(-r * T) - BarrierPrice), 2);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                    }
                    p1 = (sum / trials) * Math.Exp(-r * T);
                    SE = Math.Sqrt(sum / (trials - 1) / trials);
                }

                return SE;
            }
            else
            {
                Simulator sims = new Simulator();

                double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                double BarrierPrice = opt2.BarrierPrice(S0, K, r, T, sigma, trials, N, barrier, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread, KnockIn);
                double sum = 0;

                double[,] A1 = new double[trials, N + 1];
                double[] A_trials1 = new double[trials];

                double[,] A2 = new double[trials, N + 1];
                double[] A_trials2 = new double[trials];

                double dt = T / N;

                double nudt = (r - 0.5 * sigma * sigma) * dt;

                double sigsdt = sigma * Math.Sqrt(dt);

                double erddt = Math.Exp(r * dt);

                double beta1 = -1;

                double sum_CT = 0;
                double sum_CT2 = 0;
                double[] sum_CT_array = new double[trials];
                double[] sum_CT2_array = new double[trials];

                double p1 = 0;
                double SE = 0;

                double a, b;

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] < barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] > barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] < barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] > barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }

                if (useCV == true && isAnti == true)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St2 - K) + beta1 * cv2);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St1, 0) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St2, 0) + beta1 * cv2);
                            }
                        }

                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    SE = SD / Math.Sqrt(trials);

                }
                else if (useCV == true && isAnti == false)
                {

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {

                        double St = S0;
                        double cv = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = Math.Max(St - K, 0) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = Math.Max(K - St, 0) + beta1 * cv;
                            }
                        }
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    SE = SD / Math.Sqrt(trials);
                }
                else if (useCV == false && isAnti == true)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        a = 0;
                        b = 0;
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                a = sim1[i, N] - K;
                            }
                            if (A_trials2[i] == 1)
                            {
                                b = sim2[i, N] - K;
                            }
                            sum = sum + Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - BarrierPrice), 2);
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                a = -sim1[i, N] + K;
                            }
                            if (A_trials2[i] == 1)
                            {
                                b = -sim2[i, N] + K;
                            }
                            sum = sum + Math.Pow(((Math.Max(a, 0) + Math.Max(b, 0)) / 2 * Math.Exp(-r * T) - BarrierPrice), 2);
                        }
                    }
                    p1 = (sum / trials / 2) * Math.Exp(-r * T);
                    SE = Math.Sqrt(sum / (trials - 1) / trials);
                }
                else if (useCV == false && isAnti == false)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Pow((Math.Max(sim1[i, N] - K, 0) * Math.Exp(-r * T) - BarrierPrice), 2);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Pow((Math.Max(K - sim1[i, N], 0) * Math.Exp(-r * T) - BarrierPrice), 2);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                    }
                    p1 = (sum / trials) * Math.Exp(-r * T);
                    SE = Math.Sqrt(sum / (trials - 1) / trials);
                }

                return SE;
            }
        }

        
        // The function to calculate Delta
        public double LookbackOption_Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {

                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                        return Delta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }
                    else
                    {
                        // Define the variables for calculating the delta value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;


                        // Using the new simulation to calculate the option price
                        double Delta_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Delta_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculating the delta value
                        double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                        return Delta;
                    }

                }
            }
        }

        //The function to calculate Gamma
        public double LookbackOption_Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;

                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }
                    else
                    {
                        // Define the variables for calculating the gamma value
                        double dS0 = S0 * 0.001;
                        double S0a = S0 + dS0;
                        double S0m = S0 - dS0;

                        // Using the new simulation to calculate the option price
                        double Gamma_p1 = opt2.lookBackPrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p2 = opt2.lookBackPrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Gamma_p3 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the gamma value
                        double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                        return Gamma;
                    }

                }
            }
        }

        // The function to calculate Vega
        public double LookbackOption_Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }
                    else
                    {
                        // Define the variables for calculating the vega value
                        double dsigma = sigma * 0.001;
                        double sigmaa = sigma + dsigma;
                        double sigmam = sigma - dsigma;

                        // Using the new simulation to calculate the option price
                        double Vega_p1 = opt2.lookBackPrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Vega_p2 = opt2.lookBackPrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the vega value
                        double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                        return Vega;
                    }


                }
            }
        }

        // The function to calculate Theta
        public double LookbackOption_Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                        return Theta_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }
                    else
                    {
                        // Define the variables for calculating the theta value
                        double dT = T * 0.001;
                        double Tm = T - dT;

                        // Using the new simulation to calculate the option price
                        double Theta_p1 = opt2.lookBackPrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Theta_p2 = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Theta = (Theta_p1 - Theta_p2) / dT;

                        return Theta;
                    }

                }
            }
        }

        // The function to calculate Rho
        public double LookbackOption_Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = true, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho_Antithetic;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }
                    else
                    {
                        // Define the variables for calculating the rho value
                        double dr = r * 0.001;
                        double ra = r + dr;
                        double rm = r - dr;

                        // Using the new simulation to calculate the option price
                        double Rho_p1 = opt2.lookBackPrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);
                        double Rho_p2 = opt2.lookBackPrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, useCV = false, numOfThread);

                        // Calculate the theta value
                        double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                        return Rho;
                    }

                }
            }
        }

        // The function to calculate SE
        public double LookbackOption_SE(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    double[,] sim1 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            double max1 = sim1[i, 0];
                            double max2 = sim1[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sim1[i, j] >= max1)
                                {
                                    max1 = sim1[i, j];
                                }
                            }

                            for (int j = 0; j < N; j++)
                            {
                                if (sim2[i, j] >= max2)
                                {
                                    max2 = sim2[i, j];
                                }
                            }

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(max1 - K, 0) + beta1 * cv1) + (Math.Max(max2 - K, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                    else
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            double St1 = S0;
                            double St2 = S0;
                            double cv1 = 0;
                            double cv2 = 0;

                            double min1 = sim1[i, 0];
                            double min2 = sim1[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sim1[i, j] <= min1)
                                {
                                    min1 = sim1[i, j];
                                }
                            }

                            for (int j = 0; j < N; j++)
                            {
                                if (sim2[i, j] <= min2)
                                {
                                    min2 = sim2[i, j];
                                }
                            }

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                                double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                                double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                                double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                                cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                                cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                                St1 = Stn1;
                                St2 = Stn2;

                            }

                            double CT = 0.5 * ((Math.Max(K - min1, 0) + beta1 * cv1) + (Math.Max(K - min2, 0) + beta1 * cv2));
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }

                }
                else
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    if (isput == false)
                    {
                        
                        for (int i = 0; i < trials; i++)
                        {
                            
                            double max = sims[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims[i, j] >= max)
                                {
                                    max = sims[i, j];
                                }
                            }
                            double St = S0;
                            double cv = 0;

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(max - K, 0) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double call_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                    else
                    {

                        for (int i = 0; i < trials; i++)
                        {
                            double St = S0;
                            double cv = 0;

                            double min = sims[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims[i, j] <= min)
                                {
                                    min = sims[i, j];
                                }
                            }

                            for (int j = 0; j < N; j++)
                            {

                                double t = (j - 1) * dt;

                                // double[,] Stn = new double[trials, N];
                                double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                                double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                                cv = cv + delta * (Stn - St * erddt);

                                St = Stn;

                            }

                            double CT = Math.Max(0, K - min) + beta1 * cv;
                            sum_CT = sum_CT + CT;
                            sum_CT2 = sum_CT2 + CT * CT;

                        }
                        double put_value = sum_CT / trials * Math.Exp(-r * T);
                        double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                        double SE = SD / Math.Sqrt(trials);

                        return SE;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {

                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sim1 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                    double LookbackPrice = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);


                    for (int i = 0; i < trials; i++)
                    {
                        double min1 = sim1[i, 0];
                        double min2 = sim1[i, 0];

                        double max1 = sim1[i, 0];
                        double max2 = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min1)
                            {
                                min1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] <= min2)
                            {
                                min2 = sim2[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max1)
                            {
                                max1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] >= max2)
                            {
                                max2 = sim2[i, j];
                            }
                        }

                        if (isput == false)
                        {
                            sums = sums + Math.Pow(((Math.Max(max1 - K, 0) * Math.Exp(-r * T) + Math.Max(max2 - K, 0)) / 2 - LookbackPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow(((Math.Max(K - min1, 0) * Math.Exp(-r * T) + Math.Max(K - min2, 0)) / 2 - LookbackPrice), 2);
                        }

                        // sums = sums + Math.Pow(((sims[a, N] * Math.Exp(-r * T) + sims2[a, N] * Math.Exp(-r * T)) / 2 - EUPrice), 2);
                    }

                    double SD = Math.Sqrt(sums / (trials - 1));

                    return SD / Math.Sqrt(trials);
                }
                else
                {
                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double LookbackPrice = opt2.lookBackPrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);


                    for (int i = 0; i < trials; i++)
                    {
                        double min = sims[i, 0];
                        double max = sims[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] <= min)
                            {
                                min = sims[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] >= max)
                            {
                                max = sims[i,j];
                            }
                        }

                        if (isput == false)
                        {
                            sums = sums + Math.Pow((Math.Max(max - K, 0) * Math.Exp(-r * T) - LookbackPrice), 2);
                        }
                        else
                        {
                            sums = sums + Math.Pow((Math.Max(K - min, 0) * Math.Exp(-r * T) - LookbackPrice), 2);
                        }

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }
            }
        }
    
        // The function to calculate Delta
        public double RangeOption_Delta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the delta value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;


                    // Using the new simulation to calculate the option price
                    double Delta_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                    double Delta_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                    // Calculating the delta value
                    double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                    return Delta_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the delta value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;


                    // Using the new simulation to calculate the option price
                    double Delta_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Delta_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculating the delta value
                    double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                    return Delta;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the delta value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;


                    // Using the new simulation to calculate the option price
                    double Delta_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Delta_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculating the delta value
                    double Delta_Antithetic = (Delta_p1 - Delta_p2) / (2 * dS0);
                    return Delta_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the delta value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;


                    // Using the new simulation to calculate the option price
                    double Delta_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Delta_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                    // Calculating the delta value
                    double Delta = (Delta_p1 - Delta_p2) / (2 * dS0);

                    return Delta;
                }
            }
        }

        //The function to calculate Gamma
        public double RangeOption_Gamma(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the gamma value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;

                    // Using the new simulation to calculate the option price
                    double Gamma_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                    double Gamma_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                    double Gamma_p3 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                    // Calculate the gamma value
                    double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                    return Gamma_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the gamma value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;

                    // Using the new simulation to calculate the option price
                    double Gamma_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Gamma_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Gamma_p3 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculate the gamma value
                    double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                    return Gamma;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the gamma value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;

                    // Using the new simulation to calculate the option price
                    double Gamma_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Gamma_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Gamma_p3 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculate the gamma value
                    double Gamma_Antithetic = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                    return Gamma_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the gamma value
                    double dS0 = S0 * 0.001;
                    double S0a = S0 + dS0;
                    double S0m = S0 - dS0;

                    // Using the new simulation to calculate the option price
                    double Gamma_p1 = opt2.RangePrice(S0a, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Gamma_p2 = opt2.RangePrice(S0m, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Gamma_p3 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                    // Calculate the gamma value
                    double Gamma = (Gamma_p1 - 2 * Gamma_p3 + Gamma_p2) / Math.Pow(dS0, 2);

                    return Gamma;
                }
            }
        }

        // The function to calculate Vega
        public double RangeOption_Vega(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the vega value
                    double dsigma = sigma * 0.001;
                    double sigmaa = sigma + dsigma;
                    double sigmam = sigma - dsigma;

                    // Using the new simulation to calculate the option price
                    double Vega_p1 = opt2.RangePrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                    double Vega_p2 = opt2.RangePrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                    // Calculate the vega value
                    double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                    return Vega_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the vega value
                    double dsigma = sigma * 0.001;
                    double sigmaa = sigma + dsigma;
                    double sigmam = sigma - dsigma;

                    // Using the new simulation to calculate the option price
                    double Vega_p1 = opt2.RangePrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Vega_p2 = opt2.RangePrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculate the vega value
                    double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                    return Vega;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the vega value
                    double dsigma = sigma * 0.001;
                    double sigmaa = sigma + dsigma;
                    double sigmam = sigma - dsigma;

                    // Using the new simulation to calculate the option price
                    double Vega_p1 = opt2.RangePrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Vega_p2 = opt2.RangePrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculate the vega value
                    double Vega_Antithetic = (Vega_p1 - Vega_p2) / (2 * dsigma);

                    return Vega_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the vega value
                    double dsigma = sigma * 0.001;
                    double sigmaa = sigma + dsigma;
                    double sigmam = sigma - dsigma;

                    // Using the new simulation to calculate the option price
                    double Vega_p1 = opt2.RangePrice(S0, K, r, T, sigmaa, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Vega_p2 = opt2.RangePrice(S0, K, r, T, sigmam, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                    // Calculate the vega value
                    double Vega = (Vega_p1 - Vega_p2) / (2 * dsigma);

                    return Vega;
                }
            }
        }

        // The function to calculate Theta
        public double RangeOption_Theta(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the theta value
                    double dT = T * 0.001;
                    double Tm = T - dT;

                    // Using the new simulation to calculate the option price
                    double Theta_p1 = opt2.RangePrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);
                    double Theta_p2 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = true, numOfThread);

                    // Calculate the theta value
                    double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                    return Theta_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the theta value
                    double dT = T * 0.001;
                    double Tm = T - dT;

                    // Using the new simulation to calculate the option price
                    double Theta_p1 = opt2.RangePrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Theta_p2 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculate the theta value
                    double Theta = (Theta_p1 - Theta_p2) / dT;

                    return Theta;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the theta value
                    double dT = T * 0.001;
                    double Tm = T - dT;

                    // Using the new simulation to calculate the option price
                    double Theta_p1 = opt2.RangePrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Theta_p2 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculate the theta value
                    double Theta_Antithetic = (Theta_p1 - Theta_p2) / dT;

                    return Theta_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the theta value
                    double dT = T * 0.001;
                    double Tm = T - dT;

                    // Using the new simulation to calculate the option price
                    double Theta_p1 = opt2.RangePrice(S0, K, r, Tm, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Theta_p2 = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                    // Calculate the theta value
                    double Theta = (Theta_p1 - Theta_p2) / dT;

                    return Theta;
                }
            }
        }

        // The function to calculate Rho
        public double RangeOption_Rho(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the rho value
                    double dr = r * 0.001;
                    double ra = r + dr;
                    double rm = r - dr;

                    // Using the new simulation to calculate the option price
                    double Rho_p1 = opt2.RangePrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Rho_p2 = opt2.RangePrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculate the theta value
                    double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                    return Rho_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the rho value
                    double dr = r * 0.001;
                    double ra = r + dr;
                    double rm = r - dr;

                    // Using the new simulation to calculate the option price
                    double Rho_p1 = opt2.RangePrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);
                    double Rho_p2 = opt2.RangePrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = true, numOfThread);

                    // Calculate the theta value
                    double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                    return Rho;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    // Define the variables for calculating the rho value
                    double dr = r * 0.001;
                    double ra = r + dr;
                    double rm = r - dr;

                    // Using the new simulation to calculate the option price
                    double Rho_p1 = opt2.RangePrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);
                    double Rho_p2 = opt2.RangePrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, useCV = false, numOfThread);

                    // Calculate the theta value
                    double Rho_Antithetic = (Rho_p1 - Rho_p2) / (2 * dr);

                    return Rho_Antithetic;
                }
                else
                {
                    // Define the variables for calculating the rho value
                    double dr = r * 0.001;
                    double ra = r + dr;
                    double rm = r - dr;

                    // Using the new simulation to calculate the option price
                    double Rho_p1 = opt2.RangePrice(S0, K, ra, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);
                    double Rho_p2 = opt2.RangePrice(S0, K, rm, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, useCV = false, numOfThread);

                    // Calculate the theta value
                    double Rho = (Rho_p1 - Rho_p2) / (2 * dr);

                    return Rho;
                }
            }
        }

        // The function to calculate SE
        public double RangeOption_SE(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    double[,] sim1 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    for (int i = 0; i < trials; i++)
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        double min1 = sim1[i, 0];
                        double min2 = sim2[i, 0];
                        double max1 = sim1[i, 0];
                        double max2 = sim2[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min1)
                            {
                                min1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] <= min2)
                            {
                                min2 = sim2[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max1)
                            {
                                max1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] >= max2)
                            {
                                max2 = sim2[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(max1 - min1, 0) + beta1 * cv1) + (Math.Max(max2 - min2, 0) + beta1 * cv2));
                        sum_CT = sum_CT + CT;
                        sum_CT2 = sum_CT2 + CT * CT;

                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    return SE;

                }
                else
                {

                    ControlVariate CV1 = new ControlVariate();

                    double dt = T / N;

                    double nudt = (r - 0.5 * sigma * sigma) * dt;

                    double sigsdt = sigma * Math.Sqrt(dt);

                    double erddt = Math.Exp(r * dt);

                    double beta1 = -1;

                    double sum_CT = 0;
                    double sum_CT2 = 0;

                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    for (int i = 0; i < trials; i++)
                    {
                        double St = S0;
                        double cv = 0;

                        double min = sims[i, 0];
                        double max = sims[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] <= min)
                            {
                                min = sims[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] >= max)
                            {
                                max = sims[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(max - min, 0) + beta1 * cv;
                        sum_CT = sum_CT + CT;
                        sum_CT2 = sum_CT2 + CT * CT;

                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - sum_CT * (sum_CT / trials)) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    return SE;

                }
            }
            else
            {
                if (isAnti == true)
                {

                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sim1 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                    double RangePrice = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);


                    for (int i = 0; i < trials; i++)
                    {

                        double min1 = sim1[i, 0];
                        double min2 = sim2[i, 0];
                        double max1 = sim1[i, 0];
                        double max2 = sim2[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min1)
                            {
                                min1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] <= min2)
                            {
                                min2 = sim2[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max1)
                            {
                                max1 = sim1[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] >= max2)
                            {
                                max2 = sim2[i, j];
                            }
                        }
                        sums = sums + Math.Pow(((Math.Max(max1 - min1, 0) * Math.Exp(-r * T) + Math.Max(max2 - min2, 0)) / 2 - RangePrice), 2);

                        // sums = sums + Math.Pow(((sims[a, N] * Math.Exp(-r * T) + sims2[a, N] * Math.Exp(-r * T)) / 2 - EUPrice), 2);
                    }

                    double SD = Math.Sqrt(sums / (trials - 1));

                    return SD / Math.Sqrt(trials);
                }
                else
                {
                    // Define the variables for calculating the standard error value
                    double sums = 0;
                    double[,] sims = opt1.GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double RangePrice = opt2.RangePrice(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput, isAnti, isSE, useCV, numOfThread);

                    for (int i = 0; i < trials; i++)
                    {
                        double min = sims[i, 0];
                        double max = sims[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] <= min)
                            {
                                min = sims[i, j];
                            }
                        }

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i, j] >= max)
                            {
                                max = sims[i, j];
                            }
                        }
                        sums = sums + Math.Pow((Math.Max(max - min, 0) * Math.Exp(-r * T) - RangePrice), 2);

                    }

                    double SD = Math.Sqrt(sums / (trials - 1));
                    return SD / Math.Sqrt(trials);
                }
            }
        }
    

    }
}
