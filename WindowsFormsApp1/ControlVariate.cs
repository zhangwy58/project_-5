﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WindowsFormsApp1
{
    class ControlVariate
    {

        public double CV(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;
            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];

            if (isAnti == true)
            {
                if (isput == false)
                {
                    // double[] sum_CT = new double[N];
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(St1 - K, 0) + beta1 * cv1) + (Math.Max(St2 - K, 0) + beta1 * cv2));
                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
                else
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(K - St1, 0) + beta1 * cv1) + (Math.Max(K - St2, 0) + beta1 * cv2));
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }
            else
            {
                if (isput == false)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(St - K, 0) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
                else
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(0, K - St) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }

        }

        public double Asian_CV(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {
            Simulator sims = new Simulator();
            double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;
            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];

            // 
            if (isAnti == true)
            {
                if (isput == false)
                {
                    // Getting the average of St1 and St2
                    // double St_sum1 = 0.0;
                    // double St_sum2 = 0.0;

                    double[] St_average1 = new double[trials];
                    double[] St_average2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double St_sum1 = 0.0;
                        double St_sum2 = 0.0;

                        for (int j = 0; j < N; j++)
                        {
                            St_sum1 = St_sum1 + sim1[i, j];
                            St_sum2 = St_sum2 + sim2[i, j];
                        }
                        St_average1[i] = St_sum1 / N;
                        St_average2[i] = St_sum2 / N;
                    }

                    // Using parallel to calculate
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(St_average1[i] - K, 0) + beta1 * cv1) + (Math.Max(St_average2[i] - K, 0) + beta1 * cv2));

                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
                else
                {
                    // Getting the average of St1 and St2

                    double[] St_average1 = new double[trials];
                    double[] St_average2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double St_sum1 = 0.0;
                        double St_sum2 = 0.0;

                        for (int j = 0; j < N; j++)
                        {
                            St_sum1 = St_sum1 + sim1[i, j];
                            St_sum2 = St_sum2 + sim2[i, j];
                        }
                        St_average1[i] = St_sum1 / N;
                        St_average2[i] = St_sum2 / N;
                    }

                    //
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(K - St_average1[i], 0) + beta1 * cv1) + (Math.Max(K - St_average2[i], 0) + beta1 * cv2));
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }
            else
            {
                if (isput == false)
                {

                    double[] St_average = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double St_sum = 0.0;

                        for (int j = 0; j < N; j++)
                        {
                            St_sum = St_sum + sim1[i, j];
                        }
                        St_average[i] = St_sum / N;
                    }

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(St_average[i] - K, 0) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
                else
                {
                    
                    double[] St_average = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double St_sum = 0.0;
                        for (int j = 0; j < N; j++)
                        {
                            St_sum = St_sum + sim1[i, j];
                        }
                        St_average[i] = St_sum / N;
                    }

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(0, K - St_average[i]) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }
        }
        
        public double Digital_CV(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {
            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;
            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];

            double[] lastPrice1 = new double[trials];
            double[] lastPrice2 = new double[trials];

            Simulator sims = new Simulator();
            double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

            double[] payOff = new double[trials];

            if (isAnti == true)
            {
                if (isput == false)
                {
                    double sum_rebate1 = 0;
                    double sum_rebate2 = 0;

                    for (int i = 0; i < trials; i++)
                    {
                        lastPrice1[i] = sim1[i, N];
                        if (lastPrice1[i] < K)
                        {
                            sum_rebate1 = sum_rebate1 + 0;
                        }
                        else
                        {
                            sum_rebate1 = sum_rebate1 + rebate;
                        }
                        
                    }
                    double p1 = sum_rebate1 / trials;

                    for (int i = 0; i < trials; i++)
                    {
                        lastPrice2[i] = sim2[i, N];
                        if (lastPrice2[i] < K)
                        {
                            sum_rebate2 = sum_rebate2 + 0;
                        }
                        else
                        {
                            sum_rebate2 = sum_rebate2 + rebate;
                        }
                    }
                    double p2 = sum_rebate2 / trials;

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((p1 + beta1 * cv1) + (p2 + beta1 * cv2));


                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }
                }
                else
                {
                    double sum_rebate1 = 0;
                    double sum_rebate2 = 0;

                    for (int i = 0; i < trials; i++)
                    {
                        lastPrice1[i] = sim1[i, N];
                        if (lastPrice1[i] > K)
                        {
                            sum_rebate1 = sum_rebate1 + 0;
                        }
                        else
                        {
                            sum_rebate1 = sum_rebate1 + rebate;
                        }
                        
                    }
                    double p1 = sum_rebate1 / trials;

                    for (int i = 0; i < trials; i++)
                    {
                        lastPrice2[i] = sim2[i, N];
                        if (lastPrice2[i] > K)
                        {
                            sum_rebate2 = sum_rebate2 + 0;
                        }
                        else
                        {
                            sum_rebate2 = sum_rebate2 + rebate;
                        }
                        
                    }
                    double p2 = sum_rebate2 / trials;

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((p1 + beta1 * cv1) + (p2 + beta1 * cv2));


                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }
            else
            {
                if (isput == false)
                {
                    double sum_rebate = 0;

                    for (int i = 0; i < trials; i++)
                    {
                        lastPrice1[i] = sim1[i, N];

                        if (lastPrice1[i] < K)
                        {
                            sum_rebate = sum_rebate + 0;
                        }
                        else
                        {
                            sum_rebate = sum_rebate + rebate;
                        }
                        
                    }
                    double p = sum_rebate / trials;

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = p + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
                else
                {
                    double sum_rebate = 0;
                    for (int i = 0; i < trials; i++)
                    {

                        lastPrice1[i] = sim1[i, N];

                        if (lastPrice1[i] > K)
                        {
                            sum_rebate = sum_rebate + 0;
                        }
                        else
                        {
                            sum_rebate = sum_rebate + rebate;
                        }
                        
                    }
                    double p = sum_rebate / trials;

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = p + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }

                }
            }
        }

        public double lookBack_CV(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {

            Simulator sims = new Simulator();
            double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;

            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];


            if (isAnti == true)
            {
                if (isput == false)
                {
                    double[] St_max1 = new double[trials];
                    double[] St_max2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double max1 = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max1)
                            {
                                max1 = sim1[i, j];
                            }
                        }
                        St_max1[i] = max1;
                    }

                    for (int i = 0; i < trials; i++)
                    {
                        double max2 = sim2[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] >= max2)
                            {
                                max2 = sim2[i, j];
                            }
                        }
                        St_max2[i] = max2;
                    }
                    // Using parallel to calculate
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(St_max1[i] - K, 0) + beta1 * cv1) + (Math.Max(St_max2[i] - K, 0) + beta1 * cv2));

                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }
                }
                else
                {
                    double[] St_min1 = new double[trials];
                    double[] St_min2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double min1 = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min1)
                            {
                                min1 = sim1[i, j];
                            }
                        }
                        St_min1[i] = min1;
                    }

                    for (int i = 0; i < trials; i++)
                    {
                        double min2 = sim2[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] <= min2)
                            {
                                min2 = sim2[i, j];
                            }
                        }
                        St_min2[i] = min2;
                    }
                    // Using parallel to calculate
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }

                        double CT = 0.5 * ((Math.Max(K - St_min1[i], 0) + beta1 * cv1) + (Math.Max(K - St_min2[i], 0) + beta1 * cv2));

                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double call_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return call_value;
                    }
                    else
                    {
                        return SE;
                    }
                }
            }
            else
            {
                if (isput == false)
                {
                    double[] St_max = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double max = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max)
                            {
                                max = sim1[i, j];
                            }
                        }
                        St_max[i] = max;
                    }

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(0, (St_max[i] - K)) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }
                }
                else
                {
                    double[] St_min = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        double min = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min)
                            {
                                min = sim1[i, j];
                            }
                        }
                        St_min[i] = min;
                    }

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St = S0;
                        double cv = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }

                        double CT = Math.Max(0, (K - St_min[i])) + beta1 * cv;
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    double put_value = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                    if (isSE == false)
                    {
                        return put_value;
                    }
                    else
                    {
                        return SE;
                    }
                }
            }
        }

        public double Range_CV(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, int numOfThread)
        {

            Simulator sims = new Simulator();
            double[,] sim1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            double[,] sim2 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

            double dt = T / N;

            double nudt = (r - 0.5 * sigma * sigma) * dt;

            double sigsdt = sigma * Math.Sqrt(dt);

            double erddt = Math.Exp(r * dt);

            double beta1 = -1;

            double sum_CT = 0;
            double sum_CT2 = 0;

            double[] sum_CT_array = new double[trials];
            double[] sum_CT2_array = new double[trials];

            if (isAnti == true)
            {

                //
                double sum1 = 0;
                double sum2 = 0;

                //
                for (int i = 0; i < trials; i++)
                {
                    double max1 = sim1[i, 0];
                    double min1 = sim1[i, 0];

                    for (int j = 0; j < N; j++)
                    {
                        if (sim1[i, j] >= max1)
                        {
                            max1 = sim1[i, j];
                        }
                    }
                    for (int j = 0; j < N; j++)
                    {
                        if (sim1[i, j] <= min1)
                        {
                            min1 = sim1[i, j];
                        }
                    }
                    double d = max1 - min1;
                    sum1 = sum1 + d;
                }
                double p1 = sum1 / trials;

                for (int i = 0; i < trials; i++)
                {
                    double max2 = sim2[i, 0];
                    double min2 = sim2[i, 0];

                    for (int j = 0; j < N; j++)
                    {
                        if (sim2[i, j] >= max2)
                        {
                            max2 = sim2[i, j];
                        }
                    }
                    for (int j = 0; j < N; j++)
                    {
                        if (sim2[i, j] <= min2)
                        {
                            min2 = sim2[i, j];
                        }
                    }
                    double d = max2 - min2;
                    sum2 = sum2 + d;
                }
                double p2 = sum2 / trials;

                // Using parallel to calculate
                Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                {
                    double St1 = S0;
                    double St2 = S0;
                    double cv1 = 0;
                    double cv2 = 0;

                    for (int j = 0; j < N; j++)
                    {

                        double t = (j - 1) * dt;

                        // double[,] Stn = new double[trials, N];
                        double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                        double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                        double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                        double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                        St1 = Stn1;
                        St2 = Stn2;

                    }

                    double CT = 0.5 * ((p1 + beta1 * cv1) + (p2 + beta1 * cv2));

                    sum_CT_array[i] = CT;
                    sum_CT2_array[i] = CT * CT;
                });
                for (int i = 0; i < trials; i++)
                {
                    sum_CT = sum_CT + sum_CT_array[i];
                    sum_CT2 = sum_CT2 + sum_CT2_array[i];
                }
                double call_value = sum_CT / trials * Math.Exp(-r * T);
                double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                double SE = SD / Math.Sqrt(trials);

                if (isSE == false)
                {
                    return call_value;
                }
                else
                {
                    return SE;
                }
            }
            else
            {
                
                // double[,] sims1 = sims.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                double sum = 0;
                for (int i = 0; i < trials; i++)
                {

                    double max = sim1[i, 0];
                    double min = sim1[i, 0];

                    for (int j = 0; j < N; j++)
                    {
                        if (sim1[i, j] >= max)
                        {
                            max = sim1[i, j];
                        }
                    }
                    for (int j = 0; j < N; j++)
                    {
                        if (sim1[i, j] <= min)
                        {
                            min = sim1[i, j];
                        }
                    }
                    double d = max - min;
                    sum = sum + d;
                }
                double p = sum / trials;

                // Using parallel to calculate
                Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                {
                    double St1 = S0;
                    double St2 = S0;
                    double cv1 = 0;
                    double cv2 = 0;

                    for (int j = 0; j < N; j++)
                    {

                        double t = (j - 1) * dt;

                        // double[,] Stn = new double[trials, N];
                        double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                        double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                        double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                        double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                        cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                        cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                        St1 = Stn1;
                        St2 = Stn2;

                    }

                    double CT = p + beta1 * cv1;

                    sum_CT_array[i] = CT;
                    sum_CT2_array[i] = CT * CT;
                });
                for (int i = 0; i < trials; i++)
                {
                    sum_CT = sum_CT + sum_CT_array[i];
                    sum_CT2 = sum_CT2 + sum_CT2_array[i];
                }
                double call_value = sum_CT / trials * Math.Exp(-r * T);
                double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                double SE = SD / Math.Sqrt(trials);

                if (isSE == false)
                {
                    return call_value;
                }
                else
                {
                    return SE;
                }
            }
        }
    }
}
