﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

/// <summary>
///  This class is working as a option pricing method
/// </summary>
namespace WindowsFormsApp1
{
    public class Option
    {

        Simulator Simulator1 = new Simulator();
        RandomGenerator RG = new RandomGenerator();

        Stopwatch watch = new Stopwatch();

        public void scheduler()
        {
            Stopwatch watch = new Stopwatch();
            Summary sum1 = new Summary();

            watch.Start();
            sum1.Sum();
            watch.Stop();
            Program.increase(1);
            IO.myTime = watch.Elapsed.Hours.ToString() + ":" + watch.Elapsed.Minutes.ToString() + ":" + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        // This function is used to get the simulations and store it in sim
        public double[,] GetSimulations(double S0, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, int numOfThread)
        {
            double[,] sim;
            sim = Simulator1.Simulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
            return sim;
        }
    }


    public class EuroOption : Option
    {
        // Create a control variate instance 
        ControlVariate CV1 = new ControlVariate();

        // This function is used to calculate option price
        public double EUPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            
            if (useCV == true)
            {

                if (isAnti == true)
                {

                    if (isput == false)
                    {
                        double CV_P_Anti = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, numOfThread);

                        return CV_P_Anti;
                    }
                    else
                    {
                        double CV_P_Anti = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, numOfThread);

                        return CV_P_Anti;
                    }

                }
                else
                {
                    if (isput == false)
                    {
                        double CV_P = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);

                        return CV_P;
                    }
                    else
                    {
                        double CV_P = CV1.CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, numOfThread);

                        return CV_P;
                    }

                }

            }
            else
            {
                if (isAnti == true)
                {
                    // Define variables

                    double sum1 = 0;
                    double sum2 = 0;

                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sum1 += Math.Max(sims[a, N] - K, 0);
                            sum2 += Math.Max(sims2[a, N] - K, 0);
                        }
                        else
                        {
                            sum1 += Math.Max(K - sims[a, N], 0);
                            sum2 += Math.Max(K - sims2[a, N], 0);
                        }
                    }
                    return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                }
                else
                {
                    // Define variables
                    double sum = 0;
                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    for (int a = 0; a < trials; a++)
                    {
                        if (isput == false)
                        {
                            sum += Math.Max(sims[a, N] - K, 0);
                        }
                        else
                        {
                            sum += Math.Max(K - sims[a, N], 0);
                        }
                    }
                    return (sum / trials) * Math.Exp(-r * T);
                }
            }

        }

        public double AsianPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        double AsianCV_P_Anti = CV1.Asian_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, numOfThread);

                        return AsianCV_P_Anti;
                    }
                    else
                    {
                        double AsianCV_P_Anti = CV1.Asian_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, numOfThread);

                        return AsianCV_P_Anti;
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        double AsianCV_P = CV1.Asian_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);

                        return AsianCV_P;
                    }
                    else
                    {
                        double AsianCV_P = CV1.Asian_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);

                        return AsianCV_P;
                    }

                }

            }
            else
            {
                if (isAnti == true)
                {
                    // Define variables

                    double sum1 = 0;
                    double sum2 = 0;
                    double price1 = 0.0;
                    double price2 = 0.0;

                    double[,] sims1 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    double St_sum1 = 0.0;
                    double St_sum2 = 0.0;

                    double[] St_average1 = new double[trials];
                    double[] St_average2 = new double[trials];

                    for (int i = 0; i < trials; i++)
                    {
                        St_sum1 = 0;
                        St_sum2 = 0;
                        for (int j = 0; j < N; j++)
                        {
                            St_sum1 = St_sum1 + sims1[i, j];
                            St_sum2 = St_sum2 + sims2[i, j];
                        }

                        St_average1[i] = St_sum1 / N;
                        St_average2[i] = St_sum2 / N;

                        if (isput == false)
                        {
                            price1 = Math.Max(St_average1[i] - K, 0);
                            price2 = Math.Max(St_average2[i] - K, 0);
                        }
                        else
                        {
                            price1 = Math.Max(K - St_average1[i], 0);
                            price2 = Math.Max(K - St_average2[i], 0);
                        }

                        sum1 = sum1 + price1;
                        sum2 = sum2 + price2;
                    }
                    return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);

                }
                else
                {
                    // Define variables
                    double sum = 0.0;
                    double price = 0.0;
                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[] St_average = new double[trials];

                    double St_sum = 0.0;
                    

                    for (int i = 0; i < trials; i++)
                    {
                        St_sum = 0;
                        for (int j = 0; j < N; j++)
                        {
                            St_sum = St_sum + sims[i, j];
                        }

                        St_average[i] = St_sum / N;

                        if (isput == false)
                        {
                            price = Math.Max(St_average[i] - K, 0);
                        }
                        else
                        {
                            price = Math.Max(K - St_average[i], 0);
                        }

                        sum = sum + price;
                    }
                    return (sum / trials) * Math.Exp(-r * T);

                }
            }
        }

        public double DigitalPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double rebate, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        double Digital_P_Anti = CV1.Digital_CV(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, numOfThread);
                        return Digital_P_Anti;
                    }
                    else
                    {
                        double Digital_P_Anti = CV1.Digital_CV(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, numOfThread);
                        return Digital_P_Anti;
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        double Digital_P = CV1.Digital_CV(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);
                        return Digital_P;
                    }
                    else
                    {
                        double Digital_P = CV1.Digital_CV(S0, K, r, T, sigma, trials, N, rebate, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, numOfThread);
                        return Digital_P;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {
                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sims2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    double[] lastPrice1 = new double[trials];
                    double[] lastPrice2 = new double[trials];

                    double p1 = 0;
                    double p2 = 0;

                    double sum1 = 0;
                    double sum2 = 0;

                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice1[i] = sims[i, N];
                            if (lastPrice1[i] > K)
                            {
                                p1 = rebate;
                            }
                            else
                            {
                                p1 = 0;
                            }
                            sum1 = sum1 + p1;
                        }

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice2[i] = sims2[i, N];
                            if (lastPrice2[i] > K)
                            {
                                p2 = rebate;
                            }
                            else
                            {
                                p2 = 0;
                            }
                            sum2 = sum2 + p2;
                        }
                        return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                    }
                    else
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice1[i] = sims[i, N];
                            if (lastPrice1[i] < K)
                            {
                                p1 = rebate;
                            }
                            else
                            {
                                p1 = 0;
                            }
                            sum1 = sum1 + p1;
                        }

                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice2[i] = sims2[i, N];
                            if (lastPrice2[i] < K)
                            {
                                p2 = rebate;
                            }
                            else
                            {
                                p2 = 0;
                            }
                            sum2 = sum2 + p2;
                        }
                        return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                    }
                    
                }
                else
                {

                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    double[] lastPrice = new double[trials];
                    
                    double sum = 0;
                    double p = 0;

                    if (isput == false)
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice[i] = sims[i,N];
                            if (lastPrice[i] > K)
                            {
                                p = rebate;
                            }
                            else
                            {
                                p = 0;
                            }
                            sum = sum + p;
                        }
                        return (sum / trials) * Math.Exp(-r * T);
                    }
                    else
                    {
                        for (int i = 0; i < trials; i++)
                        {
                            lastPrice[i] = sims[i, N];
                            if (lastPrice[i] < K)
                            {
                                p = rebate;
                            }
                            else
                            {
                                p = 0;
                            }
                            sum = sum + p;
                        }
                        return (sum / trials) * Math.Exp(-r * T);
                    }

                }
            }
        }      

        public double BarrierPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double barrier, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread, bool KnockIn)
        {
            if (KnockIn == true)
            {
                double[,] sim1 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                double[,] sim2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                double sum = 0;

                double[,] A1 = new double[trials, N + 1];
                double[] A_trials1 = new double[trials];

                double[,] A2 = new double[trials, N + 1];
                double[] A_trials2 = new double[trials];

                double dt = T / N;

                double nudt = (r - 0.5 * sigma * sigma) * dt;

                double sigsdt = sigma * Math.Sqrt(dt);

                double erddt = Math.Exp(r * dt);

                double beta1 = -1;

                double sum_CT = 0;
                double sum_CT2 = 0;
                double[] sum_CT_array = new double[trials];
                double[] sum_CT2_array = new double[trials];

                double p1 = 0;
                Simulator sims = new Simulator();


                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] < barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] > barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] < barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] > barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }

                if (useCV == true && isAnti == true)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }                       
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St2 - K) + beta1 * cv2);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(-St1 + K, 0) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 0)
                            {
                                CT = CT + 0.5 * (Math.Max(-St2 + K, 0) + beta1 * cv2);
                            }
                        }

                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                }
                else if (useCV == true && isAnti == false)
                {

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {

                        double St = S0;
                        double cv = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = Math.Max(St - K, 0) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                CT = Math.Max(K - St, 0) + beta1 * cv;
                            }
                        }
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);
                }
                else if (useCV == false && isAnti == true)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Max(sim1[i, N] - K, 0);
                            }
                            if (A_trials2[i] == 0)
                            {
                                sum = sum + Math.Max(sim2[i, N] - K, 0);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Max(K - sim1[i, N], 0);
                            }
                            if (A_trials2[i] == 0)
                            {
                                sum = sum + Math.Max(K - sim2[i, N], 0);
                            }
                        }
                    }
                    p1 = (sum / trials / 2) * Math.Exp(-r * T);
                }
                else if (useCV == false && isAnti == false)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Max(sim1[i, N] - K, 0);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 0)
                            {
                                sum = sum + Math.Max(K - sim1[i, N], 0);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                    }
                    p1 = (sum / trials) * Math.Exp(-r * T);
                }

                return p1;
            } 
            else
            {
                double[,] sim1 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                double[,] sim2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                double sum = 0;

                double[,] A1 = new double[trials, N + 1];
                double[] A_trials1 = new double[trials];

                double[,] A2 = new double[trials, N + 1];
                double[] A_trials2 = new double[trials];

                double dt = T / N;

                double nudt = (r - 0.5 * sigma * sigma) * dt;

                double sigsdt = sigma * Math.Sqrt(dt);

                double erddt = Math.Exp(r * dt);

                double beta1 = -1;

                double sum_CT = 0;
                double sum_CT2 = 0;
                double[] sum_CT_array = new double[trials];
                double[] sum_CT2_array = new double[trials];

                double p1 = 0;
                Simulator sims = new Simulator();


                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] < barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials1[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A1[i, j] = 1;
                            if (sim1[i, j] > barrier)
                            {
                                A1[i, j] = 0;
                            }
                            A_trials1[i] = A_trials1[i] * A1[i, j];
                        }
                    }
                }

                if (S0 >= barrier)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] < barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < trials; i++)
                    {
                        A_trials2[i] = 1;
                        for (int j = 0; j <= N; j++)
                        {
                            A2[i, j] = 1;
                            if (sim2[i, j] > barrier)
                            {
                                A2[i, j] = 0;
                            }
                            A_trials2[i] = A_trials2[i] * A2[i, j];
                        }
                    }
                }

                if (useCV == true && isAnti == true)
                {
                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {
                        double St1 = S0;
                        double St2 = S0;
                        double cv1 = 0;
                        double cv2 = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta1 = BS.BS_Delta(St1, K, r, T, sigma, t, isput);
                            double delta2 = BS.BS_Delta(St2, K, r, T, sigma, t, isput);

                            double Stn1 = St1 * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);
                            double Stn2 = St2 * Math.Exp(nudt + sigsdt * RandomNormal2[i, j]);

                            cv1 = cv1 + delta1 * (Stn1 - St1 * erddt);
                            cv2 = cv2 + delta2 * (Stn2 - St2 * erddt);

                            St1 = Stn1;
                            St2 = Stn2;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St1 - K) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(0, St2 - K) + beta1 * cv2);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St1, 0) + beta1 * cv1);
                            }
                            if (A_trials2[i] == 1)
                            {
                                CT = CT + 0.5 * (Math.Max(K - St2, 0) + beta1 * cv2);
                            }
                        }

                        //sum_CT = sum_CT + CT;
                        //sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;
                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);

                }
                else if (useCV == true && isAnti == false)
                {

                    Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
                    {

                        double St = S0;
                        double cv = 0;
                        double CT = 0;

                        for (int j = 0; j < N; j++)
                        {

                            double t = (j - 1) * dt;

                            // double[,] Stn = new double[trials, N];
                            double delta = BS.BS_Delta(St, K, r, T, sigma, t, isput);

                            double Stn = St * Math.Exp(nudt + sigsdt * RandomNormal1[i, j]);

                            cv = cv + delta * (Stn - St * erddt);

                            St = Stn;

                        }
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = Math.Max(St - K, 0) + beta1 * cv;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                CT = Math.Max(K - St, 0) + beta1 * cv;
                            }
                        }
                        // sum_CT = sum_CT + CT;
                        // sum_CT2 = sum_CT2 + CT * CT;
                        sum_CT_array[i] = CT;
                        sum_CT2_array[i] = CT * CT;

                    });
                    for (int i = 0; i < trials; i++)
                    {
                        sum_CT = sum_CT + sum_CT_array[i];
                        sum_CT2 = sum_CT2 + sum_CT2_array[i];
                    }
                    p1 = sum_CT / trials * Math.Exp(-r * T);
                    double SD = Math.Sqrt((sum_CT2 - (sum_CT * sum_CT) / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                    double SE = SD / Math.Sqrt(trials);
                }
                else if (useCV == false && isAnti == true)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Max(sim1[i, N] - K, 0);
                            }
                            if (A_trials2[i] == 1)
                            {
                                sum = sum + Math.Max(sim2[i, N] - K, 0);
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Max(K - sim1[i, N], 0);
                            }
                            if (A_trials2[i] == 1)
                            {
                                sum = sum + Math.Max(K - sim2[i, N], 0);
                            }
                        }
                    }
                    p1 = (sum / trials / 2) * Math.Exp(-r * T);
                }
                else if (useCV == false && isAnti == false)
                {
                    for (int i = 0; i < trials; i++)
                    {
                        if (isput == false)
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Max(sim1[i, N] - K, 0);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                        else
                        {
                            if (A_trials1[i] == 1)
                            {
                                sum = sum + Math.Max(K - sim1[i, N], 0);
                            }
                            else
                            {
                                sum = sum + 0;
                            }
                        }
                    }
                    p1 = (sum / trials) * Math.Exp(-r * T);
                }

                return p1;
            }

        }

        public double lookBackPrice(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        double lookBackCV_P_Anti = CV1.lookBack_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, numOfThread);
                        return lookBackCV_P_Anti;
                    }
                    else
                    {
                        double lookBackCV_P_Anti = CV1.lookBack_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = true, isSE = false, numOfThread);
                        return lookBackCV_P_Anti;
                    }
                }
                else
                {
                    if (isput == false)
                    {
                        double lookBackCV_P = CV1.lookBack_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);
                        return lookBackCV_P;

                    }
                    else
                    {
                        double lookBackCV_P = CV1.lookBack_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = true, isAnti = false, isSE = false, numOfThread);
                        return lookBackCV_P;
                    }
                }
            }
            else
            {
                if (isAnti == true)
                {
                    if (isput == false)
                    {
                        double[,] sim1 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                        double[,] sim2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                        double sum1 = 0;
                        double sum2 = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            double max1 = sim1[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sim1[i, j] >= max1)
                                {
                                    max1 = sim1[i, j];
                                }
                            }
                            double p1 = max1 - K;
                            sum1 = sum1 + p1;

                        }
                        for (int i = 0; i < trials; i++)
                        {
                            double max2 = sim2[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sim2[i, j] >= max2)
                                {
                                    max2 = sim2[i, j];
                                }
                            }
                            double p2 = Math.Max((max2 - K), 0);
                            sum2 = sum2 + p2;

                        }
                        return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);

                    }
                    else
                    {
                        double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                        double[,] sims2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);
                        double sum1 = 0;
                        double sum2 = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            double min1 = sims[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims[i, j] <= min1)
                                {
                                    min1 = sims[i, j];
                                }
                            }
                            double p1 = Math.Max((K - min1), 0);
                            sum1 = sum1 + p1;

                        }
                        for (int i = 0; i < trials; i++)
                        {
                            double min2 = sims2[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims2[i, j] <= min2)
                                {
                                    min2 = sims[i, j];
                                }
                            }
                            double p2 = Math.Max((K - min2), 0);
                            sum2 = sum2 + p2;

                        }
                        return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                    }
                    
                }
                else
                {
                    if (isput == false)
                    {
                        double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                        double sum = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            double max = sims[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims[i, j] >= max)
                                {
                                    max = sims[i, j];
                                }
                            }
                            double p = Math.Max((max - K), 0);
                            sum = sum + p;

                        }
                        return (sum / trials) * Math.Exp(-r * T);

                    }
                    else
                    {
                        double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                        double sum = 0;

                        for (int i = 0; i < trials; i++)
                        {
                            double min = sims[i, 0];

                            for (int j = 0; j < N; j++)
                            {
                                if (sims[i, j] <= min)
                                {
                                    min = sims[i, j];
                                }
                            }
                            double p = Math.Max((K - min), 0);
                            sum = sum + p;

                        }
                        return (sum / trials) * Math.Exp(-r * T);
                    }
                }
            }
        }

        public double RangePrice(double S0, double K, double r, double T, double sigma, int trials, int N, double[,] RandomNormal1, double[,] RandomNormal2, bool isput, bool isAnti, bool isSE, bool useCV, int numOfThread)
        {
            if (useCV == true)
            {
                if (isAnti == true)
                {
                    double RangeCV_P_Anti = CV1.Range_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = true, isSE = false, numOfThread);
                    return RangeCV_P_Anti;
                }
                else
                {
                    double RangeCV_P = CV1.Range_CV(S0, K, r, T, sigma, trials, N, RandomNormal1, RandomNormal2, isput = false, isAnti = false, isSE = false, numOfThread);
                    return RangeCV_P;
                }
            }
            else
            {
                if (isAnti == true)
                {
                    double[,] sim1 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);
                    double[,] sim2 = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal2, numOfThread);

                    //
                    double sum1 = 0;
                    double sum2 = 0;

                    //
                    for (int i = 0; i < trials; i++)
                    {
                        double max1 = sim1[i, 0];
                        double min1 = sim1[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] >= max1)
                            {
                                max1 = sim1[i, j];
                            }
                        }
                        for (int j = 0; j < N; j++)
                        {
                            if (sim1[i, j] <= min1)
                            {
                                min1 = sim1[i, j];
                            }
                        }
                        double p1 = max1 - min1;
                        sum1 = sum1 + p1;
                    }

                    for (int i = 0; i < trials; i++)
                    {
                        double max2 = sim2[i, 0];
                        double min2 = sim2[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i,j] >= max2)
                            {
                                max2 = sim2[i, j];
                            }
                        }
                        for (int j = 0; j < N; j++)
                        {
                            if (sim2[i, j] <= min2)
                            {
                                min2 = sim2[i, j];
                            }
                        }
                        double p2 = max2 - min2;
                        sum2 = sum2 + p2;
                    }
                    return 0.5 * ((sum1 / trials) + (sum2 / trials)) * Math.Exp(-r * T);
                }
                else
                {
                    
                    double[,] sims = GetSimulations(S0, r, T, sigma, trials, N, RandomNormal1, numOfThread);

                    double sum = 0;

                    for (int i = 0; i < trials; i++)
                    {
                        double max = sims[i, 0];
                        double min = sims[i, 0];

                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i,j] >= max)
                            {
                                max = sims[i,j];
                            }
                        }
                        for (int j = 0; j < N; j++)
                        {
                            if (sims[i,j] <= min)
                            {
                                min = sims[i, j];
                            }
                        }
                        double p = max - min;
                        sum = sum + p;
                    }
                    return (sum / trials) * Math.Exp(-r * T);
                }
            }
        }


    }
}
