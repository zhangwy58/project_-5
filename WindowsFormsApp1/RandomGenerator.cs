﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
/// <summary>
/// This class is used to get the random variables
/// </summary>
namespace WindowsFormsApp1

{
    public class RandomGenerator
    {

        // Generate a random variables matrix
        Random rnd = new Random();

        int numOfThread = System.Environment.ProcessorCount;

        public double[,] RandomNormal(int trials, int N, int numOfThread)
        {
            // Create an array to store random variables
            double[,] RandNormal = new double[trials, N];

            // Using 
            Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, a =>

            {
                for (int b = 0; b < N; b++)
                {
                    RandNormal[a, b] = PolarRejection(rnd);
                }
            });
            return RandNormal;

        }

        public double[,] NegtRandomNormal(int trials, int N, double[,]RN1, int numOfThread)
        {
            double[,] RN2 = new double[trials, N];

            // for (int i = 0; i < trials; i++)
            Parallel.ForEach(IEnum.Step(0, trials, 1), new ParallelOptions { MaxDegreeOfParallelism = numOfThread }, i =>
            {
                for (int j = 0; j < N; j++)
                {
                    RN2[i, j] = -RN1[i, j];

                }
            });
            return RN2;

        }

        // Using Polar Rejection Method to create normal random matrix
        static double PolarRejection(Random rnd)
        {
            // Define some varirables
            double w, x1, x2, PR1;
            do
            {
                // Generate two uniform random values between 0 and 1

                lock (rnd) x1 = 1.0 - 2 * rnd.NextDouble();
                lock (rnd) x2 = 1.0 - 2 * rnd.NextDouble();
                w = Math.Pow(x1, 2) + Math.Pow(x2, 2);
            }
            while (w > 1);
            {

                double c = Math.Sqrt(-2 * Math.Log(w) / w);
                PR1 = c * x1;

            }
            return PR1;
        }

    }
}
